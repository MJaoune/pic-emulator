/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
 
#include "memory.h"

Memory *MEM_Init(MemoryType t, size_t sz, unsigned short banks)
{
	Memory *mem = (Memory *)malloc(sizeof(Memory));
	mem->type = t;
	mem->size = sz;
	mem->bankCount = banks;

	mem->data = (MemoryRegion ***)malloc(sizeof(MemoryRegion **) * banks);
	
	for(size_t i = 0; i < banks; i++)
	{		
		mem->data[i] = (MemoryRegion **)malloc(sizeof(MemoryRegion *) * sz);
		
		for(size_t j = 0; j < sz; j++)
		{
			mem->data[i][j] = (MemoryRegion *)malloc(sizeof(MemoryRegion));
			mem->data[i][j]->data = 0;
		}
	}

	return mem;
}

///Takes a memory object and returns an array of locations of the given bind type.
///addressArray is a 2D array where each bank has its own row of findings.
///arraySize is a normal array.
///Returns true if at least 1 region of specified type was found.
bool MEM_findRegion(Memory *m, BindType t, size_t ***addressArray, size_t **arraySize)
{
	if(m == NULL)
		return false;
		
	bool found = false;
	
	*arraySize = (size_t *)malloc(sizeof(size_t) * m->bankCount);
	//*arraySize = (size_t *)malloc(sizeof(size_t));
	
	for(size_t i = 0; i < m->bankCount; i++)
		(*arraySize)[i] = 0;
		
	*addressArray = (size_t **)malloc(sizeof(size_t *) * m->bankCount);
	
	for(size_t i = 0; i < m->bankCount; i++)
	{
		int c = 0;
		for(size_t j = 0; j < m->size; j++)
		{
			if(m->data[i][j]->type == t)
			{
				(*arraySize)[i]++;
				if(c == 0)
					(*addressArray)[i] = (size_t *)malloc(sizeof(size_t) * (*arraySize)[i]);
				else
					(*addressArray)[i] = (size_t *)realloc((*addressArray)[i], sizeof(size_t) * (*arraySize)[i]);
				
				(*addressArray)[i][(*arraySize)[i] - 1] = j;
				
				found = true;
				c++;
			}
		}
	}
	
	return found;
}

///Tests if a given address has the same memory region (Shadows) across the given bank range.
///For example, the work register is shadowed across all memory banks. So only one copy of it exists.
bool MEM_isShadow(Memory *m, size_t addr, size_t fromBank, size_t toBank)
{
	if(m == NULL)
		return false;
		
	if(toBank >= m->bankCount || fromBank >= toBank)
		return false;
	
	MemoryRegion *pointer = m->data[fromBank][addr];	
	
	for(size_t i = fromBank + 1; i < toBank; i++)
	{
		if(m->data[i][addr] == pointer)
			return true;
	}
	
	return false;
}

///Returns currently selected bank
unsigned char RAM_getSelectedBank(Memory *ram, PIC_MODEL model)
{
	unsigned char bank = 0;
	
	//###########################################
	//P16F84A
	if(model == P16F84A)
	{
		//Get currently selected bank
		unsigned char status = 0;
		size_t status_Addr = 0;
		MEM_getRegionData(STATUS, ram, 0, 0, status, status_Addr);
		
		//This differs from PIC to another
		status &= 0x20;	//We only need 6th bit
		status >>= 5;
		
		bank = status;
	}
	//###########################################
	
	return bank;
}

///Set flag in status register
void RAM_setFlag(Memory *ram, unsigned int f, PIC_MODEL model)
{
	//###########################################
	//P16F84A
	if(model == P16F84A)
	{
		unsigned char status = 0;
		size_t status_Addr = 0;
		MEM_getRegionData(STATUS, ram, 0, 0, status, status_Addr);
		
		if(f & TO)
			ram->data[0][status_Addr]->data |= 16;
		
		if(f & PD)
			ram->data[0][status_Addr]->data |= 8;
			
		if(f & ZERO)
			ram->data[0][status_Addr]->data |= 4;
			
		if(f & AUXILIARY)
			ram->data[0][status_Addr]->data |= 2;
			
		if(f & CARRY)
			ram->data[0][status_Addr]->data |= 1;
	}
	//###########################################
}

///Clear flag in statusr register
void RAM_clearFlag(Memory *ram, unsigned int f, PIC_MODEL model)
{
	//###########################################
	//P16F84A
	if(model == P16F84A)
	{
		unsigned char status = 0;
		size_t status_Addr = 0;
		MEM_getRegionData(STATUS, ram, 0, 0, status, status_Addr);
		
		if(f & TO)
			ram->data[0][status_Addr]->data &= (16 ^ 0xFF);
		
		if(f & PD)
			ram->data[0][status_Addr]->data &= (8 ^ 0xFF);
			
		if(f & ZERO)
			ram->data[0][status_Addr]->data &= (4 ^ 0xFF);
			
		if(f & AUXILIARY)
			ram->data[0][status_Addr]->data &= (2 ^ 0xFF);
			
		if(f & CARRY)
			ram->data[0][status_Addr]->data &= (1 ^ 0xFF);
	}
	//###########################################
}

///Write to memory (Takes care of indirect addressing)
void RAM_Write(Memory *ram, size_t addr, unsigned char d, PIC_MODEL model)
{
	//###########################################
	//P16F84A
	if(model == P16F84A)
	{
		unsigned char bank = RAM_getSelectedBank(ram, model);
		unsigned char data = d;
		
		#ifdef DEBUG
		printf("Bank: %d | Type: %d | Data: %d\n", bank, ram->data[bank][addr]->type, data);
		#endif
		
		if(ram->data[bank][addr]->type == UNDEFINED)
		{
			#ifdef DEBUG
			printf("Tried accessing undefined region.\n");
			#endif
			return;
		}
			
		//Check if FSR
		if(ram->data[bank][addr]->type == INDIRECT_ADDRESS)
		{
			//Find fsr indirect addr		
			unsigned char indirectAddress = 0;
			size_t fsr_Addr = 0;
			MEM_getRegionData(FSR, ram, 0, 0, indirectAddress, fsr_Addr);
			
			//Write
			ram->data[bank][indirectAddress]->data = d;
			ram->data[bank][addr]->data = d;
			
			return;
		}
		
		//Protect EECON1 bits
		if(ram->data[bank][addr]->type == EECON1)
		{
			printf("Accessing EECON\n");
			if((ram->data[bank][addr]->data & 1) != 0)
				data |= 1;
				
			if((ram->data[bank][addr]->data & 2) != 0)
				data |= 2;
		}
		
		//Handling Ports
		if(ram->data[bank][addr]->type == PORTA)
		{
			//Find TRIS		
			unsigned char tris = 0;
			size_t tris_Addr = 0;
			MEM_getRegionData(TRISA, ram, bank+1, 0, tris, tris_Addr);
			
			data = (d & (tris ^ 0xFF));
			
			ram->data[bank][addr]->data &= tris; //We only want to keep the data we already have and don't want to edit
			ram->data[bank][addr]->data |= data; //Write data
			
			return;
		}
		else if(ram->data[bank][addr]->type == PORTB)
		{
			//Find TRIS		
			unsigned char tris = 0;
			size_t tris_Addr = 0;
			MEM_getRegionData(TRISB, ram, bank+1, 0, tris, tris_Addr);
			
			data = (d & (tris ^ 0xFF));
			
			ram->data[bank][addr]->data &= tris; //We only want to keep the data we already have and don't want to edit
			ram->data[bank][addr]->data |= data; //Write data
			
			return;
		}
		else if(ram->data[bank][addr]->type == PORTC)
		{
			//Find TRIS		
			unsigned char tris = 0;
			size_t tris_Addr = 0;
			MEM_getRegionData(TRISC, ram, bank+1, 0, tris, tris_Addr);
			
			data = (d & (tris ^ 0xFF));
			
			ram->data[bank][addr]->data &= tris; //We only want to keep the data we already have and don't want to edit
			ram->data[bank][addr]->data |= data; //Write data
			
			return;
		}
		else if(ram->data[bank][addr]->type == PORTD)
		{
			//Find TRIS		
			unsigned char tris = 0;
			size_t tris_Addr = 0;
			MEM_getRegionData(TRISD, ram, bank+1, 0, tris, tris_Addr);
			
			data = (d & (tris ^ 0xFF));
			
			ram->data[bank][addr]->data &= tris; //We only want to keep the data we already have and don't want to edit
			ram->data[bank][addr]->data |= data; //Write data
			
			return;
		}
		else if(ram->data[bank][addr]->type == PORTE)
		{
			//Find TRIS		
			unsigned char tris = 0;
			size_t tris_Addr = 0;
			MEM_getRegionData(TRISE, ram, bank+1, 0, tris, tris_Addr);
			
			data = (d & (tris ^ 0xFF));
			
			ram->data[bank][addr]->data &= tris; //We only want to keep the data we already have and don't want to edit
			ram->data[bank][addr]->data |= data; //Write data
			
			return;
		}
		else if(ram->data[bank][addr]->type == PORTF)
		{
			//Find TRIS		
			unsigned char tris = 0;
			size_t tris_Addr = 0;
			MEM_getRegionData(TRISF, ram, bank+1, 0, tris, tris_Addr);
			
			data = (d & (tris ^ 0xFF));
			
			ram->data[bank][addr]->data &= tris; //We only want to keep the data we already have and don't want to edit
			ram->data[bank][addr]->data |= data; //Write data
			
			return;
		}
		
		ram->data[bank][addr]->data = data;
	}
	//###########################################
}

///Read from memory (Takes care of indirect addressing)
unsigned char RAM_Read(Memory *ram, size_t addr, PIC_MODEL model)
{
	unsigned char data = 0;
	//###########################################
	//P16F84A
	if(model == P16F84A)
	{
		unsigned char bank = RAM_getSelectedBank(ram, model);
		
		//Undefined area
		if(ram->data[bank][addr]->type == UNDEFINED)
		{
			#ifdef DEBUG
			printf("Tried accessing undefined region.\n");
			#endif
			return 0;
		}
		
		//Check if FSR
		if(ram->data[bank][addr]->type == INDIRECT_ADDRESS)
		{
			//Find fsr indirect addr		
			unsigned char indirectAddress = 0;
			size_t fsr_Addr = 0;
			MEM_getRegionData(FSR, ram, 0, 0, indirectAddress, fsr_Addr);

			data = ram->data[bank][indirectAddress]->data;
		}
		else
			data = ram->data[bank][addr]->data;
		
		//Handling Ports
		if(ram->data[bank][addr]->type == PORTA)
		{
			//Find TRIS		
			unsigned char tris = 0;
			size_t tris_Addr = 0;
			MEM_getRegionData(TRISA, ram, bank+1, 0, tris, tris_Addr);
			
			data &= tris;
		}
		else if(ram->data[bank][addr]->type == PORTB)
		{
			//Find TRIS		
			unsigned char tris = 0;
			size_t tris_Addr = 0;
			MEM_getRegionData(TRISB, ram, bank+1, 0, tris, tris_Addr);
			
			data &= tris;
		}
		else if(ram->data[bank][addr]->type == PORTC)
		{
			//Find TRIS		
			unsigned char tris = 0;
			size_t tris_Addr = 0;
			MEM_getRegionData(TRISC, ram, bank+1, 0, tris, tris_Addr);
			
			data &= tris;
		}
		else if(ram->data[bank][addr]->type == PORTD)
		{
			//Find TRIS		
			unsigned char tris = 0;
			size_t tris_Addr = 0;
			MEM_getRegionData(TRISD, ram, bank+1, 0, tris, tris_Addr);
			
			data &= tris;
		}
		else if(ram->data[bank][addr]->type == PORTE)
		{
			//Find TRIS		
			unsigned char tris = 0;
			size_t tris_Addr = 0;
			MEM_getRegionData(TRISE, ram, bank+1, 0, tris, tris_Addr);
			
			data &= tris;
		}
		else if(ram->data[bank][addr]->type == PORTF)
		{
			//Find TRIS		
			unsigned char tris = 0;
			size_t tris_Addr = 0;
			MEM_getRegionData(TRISF, ram, bank+1, 0, tris, tris_Addr);
			
			data &= tris;
		}
	}
	//###########################################
	
	return data;
}

