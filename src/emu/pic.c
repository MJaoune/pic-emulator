/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
 
#include "pic.h"
#include "init.h"
#include "eeprom.h"
#include "interpreter/pic16f84a.h"

///Initialize a pic
PIC *PIC_Init(PIC_MODEL model, size_t clock, InstructionSet iset, size_t ramSize, unsigned short ramBanks, size_t romSize, unsigned short romBanks, size_t eepromSize, unsigned short eepromBanks, size_t stackSize, size_t timerCount)
{
	PIC *pic = (PIC *)malloc(sizeof(PIC));
	
	pic->model = model;
	
	//Pre initialize (Usually timers)
	pic->timers = (Timer **)malloc(sizeof(Timer *) * timerCount);
	switch(model)
	{
		case P16F84A:
			P16F84A_preInit(pic->timers);
			break;
		default:
			break;
	}
	
	pic->cpu = CPU_Init(clock, iset);
	pic->RAM = MEM_Init(RAM, ramSize, ramBanks);
	pic->ROM = MEM_Init(ROM, romSize, romBanks);
	pic->EEPROM = MEM_Init(STORAGE, eepromSize, eepromBanks);
	
	pic->interrupts = 0;
	
	pic->stack = (Stack *)malloc(sizeof(Stack));
	STACK_Init(pic->stack, stackSize);
	
	pic->timerCount = timerCount;
		
	pic->watchdog = 0;
	pic->work = 0;
	pic->state = 0;
	
	//Assign post init function pointer (Should be called after this function)
	switch(model)
	{
		case P16F84A:
			pic->PIC_postInit = &P16F84A_postInit;
			break;
		default:
			pic->PIC_postInit = NULL;
	}

	return pic;
}

///Installs compiled program to ROM
void PIC_installProgram(PIC *pic, unsigned char *program, size_t programSize)
{
	if(programSize > pic->ROM->size)
		return;
		
	for(size_t i = 0; i < programSize; i++)
		pic->ROM->data[0][i]->data = program[i];
		
	pic->ROM->size = programSize;
}

///Start emulation
void PIC_Run(PIC *pic)
{
	printf("Emulation started...\n\n");
	
	unsigned long cycle = 0;
	//###########################################
	//P16F84A
	if(pic->model == P16F84A)
	{
		while(1)
		{
			if(pic->state != 2)
				continue;
				
			#ifdef DEBUG
			printf("Cycle %ld\n==================\n", cycle+1);
			#endif
				
			time_t cycleTime_Beginning = time(NULL);
				
			int instructionCycles = 0;
			
			//To detect PB change later
			unsigned char portb;
			size_t portb_Addr;
			MEM_getRegionData(PORTB, pic->RAM, 0, 0, portb, portb_Addr);
			
			unsigned char pb74_old = (portb & 0xF0) >> 4;
			
			//Process interrupts
			#ifdef DEBUG
			printf("Processing interrupts\n");
			#endif
			P16F84A_processInterrupts(pic);
			
			//Process EEPROM r/w
			#ifdef DEBUG
			printf("Processing EEPROM\n");
			#endif
			bool EEPROMInterrupt = EEPROM_Update(pic);
			if(EEPROMInterrupt)
				pic->interrupts |= INTERRUPT_EE;
			
			#ifdef DEBUG
			printf("Processing instruction\n");
			printf("Bank: %d\n", RAM_getSelectedBank(pic->RAM, pic->model));
			#endif
			//Process next instruction
			instructionCycles = P16F84A_processNextInstruction(pic);
			
			if(!instructionCycles)
			{
				printf("Execution ended.\n");
				pic->state = 0;
				break;
			}
			else if(instructionCycles == -1)
			{
				printf("Execution went out of bounds.\n");
				pic->state = 0;
				break;
			}
			
			unsigned char intcon;
			size_t intcon_Addr;
			MEM_getRegionData(INTCON, pic->RAM, 0, 0, intcon, intcon_Addr);
			
			//Process timers
			#ifdef DEBUG
			printf("Processing timer\n");
			#endif
			
			//Calculate prescaler
			unsigned char optionReg;
			size_t optionReg_Addr;
			MEM_getRegionData(OPTION, pic->RAM, 1, 0, optionReg, optionReg_Addr);
			
			unsigned char prescaler = 1;
			
			//If prescaler is set to T0 and not WDT
			if(!(optionReg & 0x08))
				 prescaler = pow(2, (int)(optionReg & 0x7)) * 2;
				
			//If clock source is PIC OSC
			if(!(optionReg & 0x20))
				pic->timers[0]->timer += 1 / prescaler;
			
			bool timerInterrupt = Timer_Sync(pic->timers[0]);
			
			if(timerInterrupt)
			{
				//Set t0 int flag
				pic->RAM->data[0][intcon_Addr]->data |= 4;
				
				//Throw interrupt
				pic->interrupts |= INTERRUPT_T0;
			}
			
			//Process PB change
			//Read again
			MEM_getRegionData(PORTB, pic->RAM, 0, 0, portb, portb_Addr);
			unsigned char pb74_new = (portb & 0xF0) >> 4;
			
			//PB [7:4] change interrupt
			if(pb74_old != pb74_new)
			{
				//Set pb74 int flag
				pic->RAM->data[0][intcon_Addr]->data |= 1;
				
				//Throw interrupt
				pic->interrupts |= INTERRUPT_PB74;
			}
			
			//Write PCL and PCLATH
			unsigned char pcl;
			size_t pcl_Addr;
			MEM_getRegionData(PCL, pic->RAM, 0, 0, pcl, pcl_Addr);
			unsigned char pclAth;
			size_t pclAth_Addr;
			MEM_getRegionData(PCLATH, pic->RAM, 0, 0, pclAth, pclAth_Addr);
			
			pic->RAM->data[0][pcl_Addr]->data = pic->cpu->pc & 0xFF;
			pic->RAM->data[0][pclAth_Addr]->data = (pic->cpu->pc & 0x3F00) >> 8;
			
			//Host-PIC Clock sync
			time_t cycleTime_End = time(NULL);
			
			double cycleElapsedTime = difftime(cycleTime_End, cycleTime_Beginning);
			double cpuPeriod = (1 / (pic->cpu->clockRate * 1000)) * instructionCycles;
			
			if(cpuPeriod > cycleElapsedTime)
			{
				unsigned long cycleNet = (double)(cpuPeriod - cycleElapsedTime) * 1000000; //Convert to microsec
				
				usleep(cycleNet); //Our PC is too fast, wait for the PIC's clock
			}
			
			cycle++;
			printf("=========================\n");
			printf("Work: 0x%02X\n\n", pic->work);
			
			printf("RAM:\n\n    ");
			for(size_t m = 0; m < 0x10; m++) printf("%02X ", m);
			printf("\n");
			for(size_t m1 = 0, r = 0; m1 < pic->RAM->bankCount; m1++)
			{
				for(size_t m2 = 0; m2 < pic->RAM->size; m2++)
				{
					if(m2 % 16 == 0) printf("\n%02X: ", r++);
					printf("%02X ", pic->RAM->data[m1][m2]->data);
				}
			}
			printf("\n=========================\n\n");
			
			#ifdef DEBUG
			printf("Timer 0: %.2f\n", pic->timers[0]->timer);
			printf("==================\n\n");
			#endif
		}
	}
	//###########################################
}
