/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "init.h"

void P16F84A_preInit(Timer **timers)
{
	timers[0] = Timer_Init();
}

void P16F84A_postInit(Memory *ram, Timer *timers)
{
	//Indirect Address
	ram->data[0][0x00]->type = INDIRECT_ADDRESS;
	free(ram->data[1][0x00]);
	ram->data[1][0x00] = ram->data[0][0x00];
	
	//T0
	ram->data[0][0x01]->type = TMR;
	timers[0].data = &(ram->data[0][0x01]->data);
	
	//Option
	ram->data[1][0x01]->type = OPTION;
	
	//PCL
	ram->data[0][0x02]->type = PCL;
	
	free(ram->data[1][0x02]);
	ram->data[1][0x02] = ram->data[0][0x02];
	
	//Status
	ram->data[0][0x03]->type = STATUS;
	
	free(ram->data[1][0x03]);
	ram->data[1][0x03] = ram->data[0][0x03];
	
	//FSR
	ram->data[0][0x04]->type = FSR;
	
	free(ram->data[1][0x04]);
	ram->data[1][0x04] = ram->data[0][0x04];
	
	//Ports
	ram->data[0][0x05]->type = PORTA;
	ram->data[1][0x05]->type = TRISA;
	
	ram->data[0][0x06]->type = PORTB;
	ram->data[1][0x06]->type = TRISB;
	
	//Undefined
	ram->data[0][0x07]->type = UNDEFINED;
	ram->data[1][0x07]->type = UNDEFINED;
	
	//EEPROM
	ram->data[0][0x08]->type = EEDATA;
	ram->data[1][0x08]->type = EECON1;
	ram->data[0][0x09]->type = EEADR;
	ram->data[1][0x09]->type = EECON2;
	
	//PCLATH
	ram->data[0][0x0A]->type = PCLATH;
	
	free(ram->data[1][0x0A]);
	ram->data[1][0x0A] = ram->data[0][0x0A];
	
	//INTCON
	ram->data[0][0x0B]->type = INTCON;
	
	free(ram->data[1][0x0B]);
	ram->data[1][0x0B] = ram->data[0][0x0B];
	
	//General purpose
	for(size_t i = 0x0C; i <= 0x4F; i++)
	{
		ram->data[0][i]->type = GENERAL;
		
		//Mirrored
		free(ram->data[1][i]);
		ram->data[1][i] = ram->data[0][i];
	}
	
	//Undefined
	/*for(size_t i = 0x50; i <= 0x7F; i++)
	{
		ram->data[0][i]->type = UNDEFINED;
		ram->data[1][i]->type = UNDEFINED;
	}*/
	
	//Initialize file
	P16F84A_fileInit(ram);
}

void P16F84A_fileInit(Memory *ram)
{
	//Initialize values
	ram->data[0][0x00]->data = 0x00;
	ram->data[0][0x01]->data = 0x00;
	ram->data[0][0x02]->data = 0x18;
	ram->data[0][0x03]->data = 0x18;
	ram->data[0][0x0A]->data = 0x00;
	ram->data[0][0x0B]->data = 0x00;
	ram->data[1][0x01]->data = 0xFF;
	ram->data[1][0x02]->data = 0x00;
	ram->data[1][0x03]->data = 0x00;
	ram->data[1][0x05]->data |= 0x1F;
	ram->data[1][0x06]->data = 0xFF;
	ram->data[1][0x08]->data = 0x00;
	
}
