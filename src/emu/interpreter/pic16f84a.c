/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "pic16f84a.h"

///Processes interrupts
void P16F84A_processInterrupts(PIC *pic)
{
	//Get INTCON
	unsigned char intcon;
	size_t intcon_Addr;
	MEM_getRegionData(INTCON, pic->RAM, 0, 0, intcon, intcon_Addr);
	
	//Check if interrupts are already enabled
	if(intcon & 128)
	{
		bool intOccurred = false;
		//EE
		if(intcon & 64)
		{
			if(pic->interrupts & INTERRUPT_EE)
			{
				printf("INT: EEPROM write complete.\n");
				intOccurred = true;
			}
		}
		
		//T0
		if(intcon & 32)
		{
			if(pic->interrupts & INTERRUPT_T0)
			{
				printf("INT: T0 overflow.\n");
				intOccurred = true;
			}
		}
		
		//PB74
		if(intcon & 8)
		{
			if(pic->interrupts & INTERRUPT_PB74)
			{
				printf("INT: Port B [7:4] has changed.\n");
				intOccurred = true;
			}
		}
		
		//Jump to 0x04
		if(intOccurred)
		{
			if(pic->model == P16F84A)
			{
				printf("INT occurred; jumping to 0x04\n");
				pic->cpu->pc = 0x04;
			}
		}
	}
}

///The most important function in the emulator
int P16F84A_processNextInstruction(PIC *pic)
{
	if(pic->cpu->pc >= pic->ROM->size)
		return 0;
		
	size_t instruction = (((pic->ROM->data[0][pic->cpu->pc]->data << 8) | pic->ROM->data[0][pic->cpu->pc + 1]->data) & 0x3FFF); //14-bit instruction
	size_t opcode = instruction;
	unsigned char cycles = 0;
	
	#ifdef DEBUG
	MISC_instructionDebug(pic, instruction);
	#endif
	
	
	//14-bit opcodes
	//###################################
	
	//RETURN
	if(opcode == 0x08)
	{
		#ifdef DEBUG
		printf("Instruction: RETURN\n");
		#endif
	
		if(pic->stack->dataCount == 0)
			//Error
			MISC_throwError(pic, "Attempting to RETURN while stack is empty.");
		else
			pic->cpu->pc = STACK_Pop(pic->stack);
	
		cycles = 2;
		return cycles;
	}
	//RETFIE
	else if(opcode == 0x09)
	{
		#ifdef DEBUG
		printf("Instruction: RETFIE\n");
		#endif
		
		if(pic->stack->dataCount == 0)
			//Error
			MISC_throwError(pic, "Attempting to RETURN while stack is empty.");
		else
			pic->cpu->pc = STACK_Pop(pic->stack);
		
		cycles = 2;
		return cycles;
	}
	//SLEEP
	else if(opcode == 0x63)
	{
		#ifdef DEBUG
		printf("Instruction: SLEEP\n");
		#endif
		pic->state = 1;
		
		//Set TO/, PD/ flags
		RAM_setFlag(pic->RAM, TO | PD, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//CLRWDT
	else if(opcode == 0x64)
	{
		#ifdef DEBUG
		printf("Instruction: CLRWDT\n");
		#endif
		pic->watchdog = 0;
		
		//Set TO/, PD/ flags
		RAM_setFlag(pic->RAM, TO | PD, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	
	//7-bit opcodes
	//###################################
	
	//opcode >>= 7;
	opcode &= 0xFF80;

	//NOP
	if(opcode == 0x00)
	{
		#ifdef DEBUG
		printf("Instruction: NOP\n");
		#endif
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//MOVWF
	else if(opcode == 0x80)
	{
		#ifdef DEBUG
		printf("Instruction: MOVWF\n");
		#endif
		size_t addr = instruction & 0x7F;
		
		//Move
		RAM_Write(pic->RAM, addr, pic->work, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//CLRW
	else if(opcode == 0x100)
	{	
		#ifdef DEBUG
		printf("Instruction: CLRW\n");
		#endif	
		//Clear work reg
		pic->work = 0;
		
		//Set Zero flag
		RAM_setFlag(pic->RAM, ZERO, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//CLRF
	else if(opcode == 0x180)
	{
		#ifdef DEBUG
		printf("Instruction: CLRF\n");
		#endif
		size_t addr = instruction & 0x7F;
		
		//Clear file reg
		RAM_Write(pic->RAM, addr, 0, pic->model);
		
		//Set Zero flag
		RAM_setFlag(pic->RAM, ZERO, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	
	//6-bit opcodes
	//###################################
	
	//opcode >>= 1;
	opcode &= 0xFF00;
	
	//ADDWF
	if(opcode == 0x700)
	{
		#ifdef DEBUG
		printf("Instruction: ADDWF\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		size_t bank = RAM_getSelectedBank(pic->RAM, pic->model);
		unsigned char operand = pic->RAM->data[bank][fileAddr]->data;
		
		//Addition
		unsigned char result;
		unsigned char flags;
		
		add8(pic->work, operand, 0, &result, &flags);
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
			
		//Carry flag
		if(flags & 1)
			RAM_setFlag(pic->RAM, CARRY, pic->model);
		else
			RAM_clearFlag(pic->RAM, CARRY, pic->model);
			
		//Auxilary flag
		if(flags & 2)
			RAM_setFlag(pic->RAM, AUXILIARY, pic->model);
		else
			RAM_clearFlag(pic->RAM, AUXILIARY, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//ANDWF
	else if(opcode == 0x500)
	{
		#ifdef DEBUG
		printf("Instruction: ANDWF\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		size_t bank = RAM_getSelectedBank(pic->RAM, pic->model);
		unsigned char operand = pic->RAM->data[bank][fileAddr]->data;
		
		unsigned char result = pic->work & operand;
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//COMF
	else if(opcode == 0x900)
	{
		#ifdef DEBUG
		printf("Instruction: COMF\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		
		unsigned char result = RAM_Read(pic->RAM, fileAddr, pic->model) ^ 0xFF;
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//DECF
	else if(opcode == 0x300)
	{
		#ifdef DEBUG
		printf("Instruction: DECF\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		
		unsigned char result = RAM_Read(pic->RAM, fileAddr, pic->model) - 1;
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//DECFSZ
	else if(opcode == 0xB00)
	{
		#ifdef DEBUG
		printf("Instruction: DECFSZ\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		printf("Save loc: %d\n", saveLocation);
		
		size_t fileAddr = instruction & 0x7F;

		unsigned char result = RAM_Read(pic->RAM, fileAddr, pic->model) - 1;
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
			
		#ifdef DEBUG
		printf("Address: %d\nResult: %d\n", fileAddr, result);
		#endif
			
		cycles = 1;
			
		//Case zero (Skip next instruction)
		if(result == 0) 
		{
			pic->cpu->pc += 2;
			cycles += 1;
		} 
		
		pic->cpu->pc += 2;
		return cycles;
	}
	//INCF
	else if(opcode == 0xA00)
	{
		#ifdef DEBUG
		printf("Instruction: INCF\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		
		unsigned char result = RAM_Read(pic->RAM, fileAddr, pic->model) + 1;
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//INCFSZ
	else if(opcode == 0xF00)
	{
		#ifdef DEBUG
		printf("Instruction: INCFSZ\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		
		unsigned char result = RAM_Read(pic->RAM, fileAddr, pic->model) + 1;
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
			
		cycles = 1;
			
		//Case zero (Skip next instruction)
		if(result == 0) 
		{
			pic->cpu->pc += 2;
			cycles += 1;
		} 
		
		pic->cpu->pc += 2;
		return cycles;
	}
	//IORWF
	else if(opcode == 0x400)
	{
		#ifdef DEBUG
		printf("Instruction: IORWF\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		size_t bank = RAM_getSelectedBank(pic->RAM, pic->model);
		unsigned char operand = pic->RAM->data[bank][fileAddr]->data;
		
		printf("save location %d\n", saveLocation);
		
		//Operation
		unsigned char result = operand | pic->work;
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//MOVF
	else if(opcode == 0x800)
	{
		#ifdef DEBUG
		printf("Instruction: MOVF\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		unsigned char bank = RAM_getSelectedBank(pic->RAM, pic->model);
		unsigned char operand = pic->RAM->data[bank][fileAddr]->data;
		
		//Move
		if(!saveLocation)
			pic->work = operand;
		else
			RAM_Write(pic->RAM, fileAddr, operand, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//RLF
	else if(opcode == 0xD00)
	{
		#ifdef DEBUG
		printf("Instruction: RLF\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		size_t bank = RAM_getSelectedBank(pic->RAM, pic->model);
		unsigned char operand = pic->RAM->data[bank][fileAddr]->data;
		
		//Operation
		unsigned char result = (operand << 1) & 0xFF;
		
		//Set carry (Since it is rotate through carry)
		if(operand & 128)
			RAM_setFlag(pic->RAM, CARRY, pic->model);
		else RAM_clearFlag(pic->RAM, CARRY, pic->model);
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//RRF
	else if(opcode == 0xC00)
	{
		#ifdef DEBUG
		printf("Instruction: RRF\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		size_t bank = RAM_getSelectedBank(pic->RAM, pic->model);
		unsigned char operand = pic->RAM->data[bank][fileAddr]->data;
		
		//Operation
		unsigned char result = (operand >> 1) & 0xFF;
		
		//Set carry (Since it is rotate through carry)
		if(operand & 1)
			RAM_setFlag(pic->RAM, CARRY, pic->model);
		else RAM_clearFlag(pic->RAM, CARRY, pic->model);
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//SUBWF
	else if(opcode == 0x200)
	{
		#ifdef DEBUG
		printf("Instruction: SUBWF\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		size_t bank = RAM_getSelectedBank(pic->RAM, pic->model);
		unsigned char operand = pic->RAM->data[bank][fileAddr]->data;
		
		//Operation
		unsigned char result;
		unsigned char flags;
		
		sub8(pic->work, operand, 0, &result, &flags);
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
			
		//Carry flag
		if(flags & 1)
			RAM_setFlag(pic->RAM, CARRY, pic->model);
		else
			RAM_clearFlag(pic->RAM, CARRY, pic->model);
			
		//Auxilary flag
		if(flags & 2)
			RAM_setFlag(pic->RAM, AUXILIARY, pic->model);
		else
			RAM_clearFlag(pic->RAM, AUXILIARY, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//SWAPF
	else if(opcode == 0xE00)
	{
		#ifdef DEBUG
		printf("Instruction: SWAPF\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		size_t bank = RAM_getSelectedBank(pic->RAM, pic->model);
		unsigned char operand = pic->RAM->data[bank][fileAddr]->data;
		
		//Operation
		unsigned char result = (operand >> 4) | (operand << 4);
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//XORWF
	else if(opcode == 0x600)
	{
		#ifdef DEBUG
		printf("Instruction: XORWF\n");
		#endif
		unsigned char saveLocation = (instruction & 128) >> 7;	//0: Work register, 1: file
		size_t fileAddr = instruction & 0x7F;
		size_t bank = RAM_getSelectedBank(pic->RAM, pic->model);
		unsigned char operand = pic->RAM->data[bank][fileAddr]->data;
		
		//Operation
		unsigned char result = operand ^ pic->work;
		
		//Save result
		if(!saveLocation)
			pic->work = result;
		else
			RAM_Write(pic->RAM, fileAddr, result, pic->model);
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//ANDLW
	else if(opcode == 0x3900)
	{
		#ifdef DEBUG
		printf("Instruction: ANDLW\n");
		#endif
		unsigned char operand = instruction & 0xFF;
		
		unsigned char result = pic->work & operand;
		
		//Save result
		pic->work = result;
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//IORLW
	else if(opcode == 0x3800)
	{
		#ifdef DEBUG
		printf("Instruction: IORLW\n");
		#endif
		unsigned char operand = instruction & 0xFF;
		
		unsigned char result = pic->work | operand;
		
		//Save result
		pic->work = result;
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//XORLW
	else if(opcode == 0x3A00)
	{
		#ifdef DEBUG
		printf("Instruction: XORLW\n");
		#endif
		unsigned char operand = instruction & 0xFF;
		
		unsigned char result = pic->work ^ operand;
		
		//Save result
		pic->work = result;
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	
	//5-bit opcodes
	//###################################
	
	//opcode >>= 1;
	opcode &= 0xFE00;
	
	//ADDLW
	if(opcode == 0x3E00)
	{
		#ifdef DEBUG
		printf("Instruction: ADDLW\n");
		#endif
		unsigned char operand = instruction & 0xFF;
		
		//Addition
		unsigned char result;
		unsigned char flags;
		
		add8(pic->work, operand, 0, &result, &flags);
		
		//Save result
		pic->work = result;
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
			
		//Carry flag
		if(flags & 1)
			RAM_setFlag(pic->RAM, CARRY, pic->model);
		else
			RAM_clearFlag(pic->RAM, CARRY, pic->model);
			
		//Auxilary flag
		if(flags & 2)
			RAM_setFlag(pic->RAM, AUXILIARY, pic->model);
		else
			RAM_clearFlag(pic->RAM, AUXILIARY, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//SUBLW
	else if(opcode == 0x3C00)
	{
		#ifdef DEBUG
		printf("Instruction: SUBLW\n");
		#endif
		unsigned char operand = instruction & 0xFF;
		
		//Addition
		unsigned char result;
		unsigned char flags;
		
		sub8(pic->work, operand, 0, &result, &flags);
		
		//Save result
		pic->work = result;
			
		//Set flags
		//Zero flag
		if(result == 0)
			RAM_setFlag(pic->RAM, ZERO, pic->model);
		else
			RAM_clearFlag(pic->RAM, ZERO, pic->model);
			
		//Carry flag
		if(flags & 1)
			RAM_setFlag(pic->RAM, CARRY, pic->model);
		else
			RAM_clearFlag(pic->RAM, CARRY, pic->model);
			
		//Auxilary flag
		if(flags & 2)
			RAM_setFlag(pic->RAM, AUXILIARY, pic->model);
		else
			RAM_clearFlag(pic->RAM, AUXILIARY, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	
	//4-bit opcodes
	//###################################
	
	//opcode >>= 1;
	opcode &= 0xFC00;
	
	//BCF
	if(opcode == 0x1000)
	{
		#ifdef DEBUG
		printf("Instruction: BCF\n");
		#endif
		size_t fileAddr = instruction & 0x7F;
		unsigned char bitIndex = (instruction >> 7) & 7;
		unsigned char bitPos = pow(2, bitIndex);
		
		//Operation
		unsigned char operand = RAM_Read(pic->RAM, fileAddr, pic->model);
		unsigned char result = operand & (bitPos ^ 0xFF);
		
		//Save result
		RAM_Write(pic->RAM, fileAddr, result, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//BSF
	else if(opcode == 0x1400)
	{
		#ifdef DEBUG
		printf("Instruction: BSF\n");
		#endif
		size_t fileAddr = instruction & 0x7F;
		unsigned char bitIndex = (instruction >> 7) & 7;
		unsigned char bitPos = pow(2, bitIndex);
		
		//Operation
		unsigned char operand = RAM_Read(pic->RAM, fileAddr, pic->model);
		unsigned char result = operand | bitPos;
		
		//Save result
		RAM_Write(pic->RAM, fileAddr, result, pic->model);
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//BTFSC
	else if(opcode == 0x1800)
	{
		#ifdef DEBUG
		printf("Instruction: BTFSC\n");
		#endif
		size_t fileAddr = instruction & 0x7F;
		unsigned char bitIndex = (instruction >> 7) & 7;
		unsigned char bitPos = pow(2, bitIndex);
		
		//Operation
		unsigned char operand = RAM_Read(pic->RAM, fileAddr, pic->model);
		unsigned char result = operand & bitPos;
		cycles = 1;
		
		//Skip
		if(!result) {
			pic->cpu->pc += 2;
			cycles += 1;
		}
		
		pic->cpu->pc += 2;
		return cycles;
	}
	//BTFSS
	else if(opcode == 0x1C00)
	{
		#ifdef DEBUG
		printf("Instruction: BTFSS\n");
		#endif
		size_t fileAddr = instruction & 0x7F;
		unsigned char bitIndex = (instruction >> 7) & 7;
		unsigned char bitPos = pow(2, bitIndex);
		
		//Operation
		unsigned char operand = RAM_Read(pic->RAM, fileAddr, pic->model);
		unsigned char result = operand & bitPos;
		cycles = 1;
		
		//Skip
		if(result) {
			pic->cpu->pc += 2;
			cycles += 1;
		}
		
		pic->cpu->pc += 2;
		return cycles;
	}
	//MOVLW
	else if(opcode == 0x3000)
	{
		#ifdef DEBUG
		printf("Instruction: MOVLW\n");
		#endif
		unsigned char operand = instruction & 0xFF;
		
		//Save result
		pic->work = operand;
		
		pic->cpu->pc += 2;
		cycles = 1;
		return cycles;
	}
	//RETLW
	else if(opcode == 0x3400)
	{
		#ifdef DEBUG
		printf("Instruction: RETLW\n");
		#endif
		unsigned char operand = instruction & 0xFF;
		
		if(pic->stack->dataCount == 0)
			//Error
			MISC_throwError(pic, "Attempting to RETURN while stack is empty.");
		else
			pic->cpu->pc = STACK_Pop(pic->stack);
			
		pic->work = operand;
	
		cycles = 2;
		return cycles;
	}
	
	//3-bit opcodes
	//###################################
	
	//opcode >>= 1;
	opcode &= 0xF800;
	
	//CALL
	if(opcode == 0x2000)
	{
		#ifdef DEBUG
		printf("Instruction: CALL\n");
		#endif
		size_t addr = instruction & 0x7FF;
		
		pic->cpu->pc = addr;
		
		if(STACK_Push(pic->stack, pic->cpu->pc + 2) == -1)
			MISC_throwError(pic, "Attempting to do CALL while stack is full.");
		
		cycles = 1;
		return cycles;
	}
	//GOTO
	else if(opcode == 0x2800)
	{
		#ifdef DEBUG
		printf("Instruction: GOTO\n");
		#endif
		size_t addr = instruction & 0x7FF;
		
		pic->cpu->pc = addr;
		
		cycles = 1;
		return cycles;
	}
	
	return -1;	//This should never be reached unless execution goes out of control.
}
