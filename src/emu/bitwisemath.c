/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "bitwisemath.h"

//Flags [0|0|0|0|0|O|DC|C/B]
///This is an 8-bit adder.
void add8(unsigned char i1, unsigned char i2, unsigned char carry, unsigned char *result, unsigned char *flags)
{
	unsigned char op1 = i1 & 0xFF;
	unsigned char op2 = i2 & 0xFF;

	unsigned char s = 0; //8-bits
	unsigned char c = carry; //Assume its 1-bit

	*flags = 0;

	unsigned char c6 = 0;
	unsigned char c7 = 0;

	//Start operation
	for(unsigned char i = 0; i < 8; ++i)
	{
		//Take next bit
		unsigned char bit1 = ((op1 & ((1 << i) & 0xFF)) >> i) & 0xFF;
		unsigned char bit2 = ((op2 & ((1 << i) & 0xFF)) >> i) & 0xFF;

		//Sum them together
		unsigned char z1 = (bit1 ^ bit2) & 0xFF;
		s |= ((c ^ z1) << i) & 0xFF;

		//Calculate carry
		unsigned char c1 = (c & z1) & 0xFF;
		unsigned char c2 = (bit1 & bit2) & 0xFF;
		c = (c1 | c2) & 0xFF;

		//Auxiliary flag set
		if(c != 0 && i == 3)
			*flags |= 0x2;

		if(i == 6) c6 = c & 1;
		else if(i == 7) c7 = c & 1;
	}

	c &= 1;

	//Check carry
	if(c == 1)
		*flags |= 0x1;
	//Check overflow
	if(c6 != c7)
		*flags |= 0x4;
	*result = s & 0xFF;
}

//Flags [0|0|0|0|0|O|DC|C/B]
///This is an 8-bit subtractor.
void sub8(unsigned char i1, unsigned char i2, unsigned char carry, unsigned char *result, unsigned char *flags)
{

	unsigned char op1 = i1 & 0xFF;
	unsigned char op2 = i2 & 0xFF;

	op2 ^= 0xFF;    //2's Complement
	op2 &= 0xFF;

	unsigned char s = 0; //8-bits
	unsigned char c = carry ^ 0xFF; //Assume its 1-bit
	c &= 1;

	*flags = 0;

	unsigned char c6 = 0;
	unsigned char c7 = 0;

	//Start operation
	for(unsigned char i = 0; i < 8; ++i)
	{
		//Take next bit
		unsigned char bit1 = ((op1 & ((1 << i) & 0xFF)) >> i) & 0xFF;
		unsigned char bit2 = ((op2 & ((1 << i) & 0xFF)) >> i) & 0xFF;

		//Sum them together
		unsigned char z1 = (bit1 ^ bit2) & 0xFF;
		s |= ((c ^ z1) << i) & 0xFF;

		//Calculate carry
		unsigned char c1 = (c & z1) & 0xFF;
		unsigned char c2 = (bit1 & bit2) & 0xFF;
		c = (c1 | c2) & 0xFF;

		//Auxiliary flag set
		if(c == 0 && i == 3)
			*flags |= 0x2;

		if(i == 6) c6 = c & 1;
		else if(i == 7) c7 = c & 1;
	}

	c ^= 0xFF;
	c &= 1;

	//Check carry
	if(c == 1)
		*flags |= 0x1;
	//Check overflow
	if(c6 != c7)
		*flags |= 0x4;

	*result = s & 0xFF;
}
