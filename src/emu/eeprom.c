/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
 
#include "eeprom.h"

///Initiates a read/write from/to the EEPROM according to EECON. Returns true on interrupt
bool EEPROM_Update(PIC *pic)
{
	bool interruptOccured = false;
	
	//###########################################
	//P16F84A
	if(pic->model == P16F84A)
	{
		//Find EECON1
		unsigned char eecon1 = 0;
		size_t eecon1_Addr = 0;
		MEM_getRegionData(EECON1, pic->RAM, 1, 0, eecon1, eecon1_Addr);
		
		//P16F84A
		if(pic->model == P16F84A)
		{
			//Read
			if(eecon1 & 1)
			{
				//Find EEADDR
				unsigned char eeadr = 0;
				size_t eeadr_Addr;
				MEM_getRegionData(EEADR, pic->RAM, 0, 0, eeadr, eeadr_Addr);
				
				//Get EEDATA address
				unsigned char eedata = 0;
				size_t eedata_Addr = 0;
				MEM_getRegionData(EEDATA, pic->RAM, 0, 0, eedata, eedata_Addr);
				
				//Initiate read
				if(eeadr < pic->EEPROM->size)
					pic->RAM->data[0][eedata_Addr]->data = pic->EEPROM->data[0][eeadr]->data;
				
				pic->RAM->data[1][eecon1_Addr]->data &= 0xFE; //Clear read bit
			}
			
			//Write
			if(eecon1 & 2)
			{
				//Find EEADDR
				unsigned char eeadr = 0;
				size_t eeadr_Addr = 0;
				MEM_getRegionData(EEADR, pic->RAM, 0, 0, eeadr, eeadr_Addr);
				
				//Get EEDATA address
				unsigned char eedata = 0;
				size_t eedata_Addr = 0;
				MEM_getRegionData(EEDATA, pic->RAM, 0, 0, eedata, eedata_Addr);
				
				#ifdef DEBUG
				printf("Writing %02X to %02X (EEPROM)\n", eedata, eeadr);
				#endif
					
				//Initiate write	
				if(eeadr < pic->EEPROM->size)
				{
					pic->EEPROM->data[0][eeadr]->data = eedata;
					pic->RAM->data[1][eecon1_Addr]->data |= 16; //Interrupt flag
					pic->RAM->data[1][eecon1_Addr]->data &= 0xF7; //No error
					interruptOccured = true;
				}
				else
				{
					//Error has occured
					pic->RAM->data[1][eecon1_Addr]->data |= 8;
					
					#ifdef DEBUG
					printf("Error writing to EEPROM\n");
					#endif
				}
				
				
				pic->RAM->data[1][eecon1_Addr]->data &= 0xFD; //Clear write bit
			}
		}
	}
	//###########################################
	
	return interruptOccured;
}
