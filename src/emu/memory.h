/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
 
#ifndef _H_MEMORY_
#define _H_MEMORY_

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "pic_model.h"

///Use the following macro instead of MEM_findRegion() unless necessary.
///Type: Memory region type
///RAM: RAM pointer
///Bank: Bank of found address
///Index: Index in address array (Usually 0)
///outData: Data in found address
///outAddress: Address of found memory region according to bank and index.
#define MEM_getRegionData(type, ram, bank, index, outData, outAddress) {\
		outData = 0;\
		outAddress = 0;\
		size_t **addr = NULL;\
		size_t *arrSize = NULL;\
		if(MEM_findRegion(ram, type, &addr, &arrSize)){\
			if(arrSize[bank] > 0) {\
			outAddress = addr[bank][index];\
			outData = ram->data[bank][addr[bank][index]]->data & 0xFF;}\
			else printf("MEM: Entry not found\n");}\
		free(addr);\
		free(arrSize);\
		}

typedef enum FLAGS {
	ZERO = 1,
	AUXILIARY = 2,
	CARRY = 4,	//Also borrow
	TO = 8,
	PD = 16
} FLAGS;

typedef enum MemoryType {
	RAM = 1,
	ROM = 2,
	STORAGE = 4	//e.g. flash memory, eeprom
} MemoryType;

///Bind type only gives us a general knowledge of the location, we handle it later.
typedef enum BindType {
	PORTA,
	PORTB,
	PORTC,
	PORTD,
	PORTE,
	PORTF,
	TRISA,
	TRISB,
	TRISC,
	TRISD,
	TRISE,
	TRISF,
	GENERAL,
	WORK,
	OPTION,
	STATUS,
	FSR,
	INDIRECT_ADDRESS,
	PCL,
	PCLATH,
	INTCON,
	TMR,
	EEDATA,
	EEADR,
	EECON1,
	EECON2,
	UNDEFINED
} BindType;

typedef struct MemoryRegion {
	unsigned char data;
	BindType type;
} MemoryRegion;

typedef struct Memory {
	MemoryType type;
	size_t size;
	unsigned short bankCount;
	MemoryRegion ***data; //2D array, depending on number of banks (Minimum banks = 1)
} Memory;

Memory *MEM_Init(MemoryType t, size_t sz, unsigned short banks);

bool MEM_findRegion(Memory *m, BindType t, size_t ***addressArray, size_t **arraySize);

bool MEM_isShadow(Memory *m, size_t addr, size_t fromBank, size_t toBank);

unsigned char RAM_getSelectedBank(Memory *ram, PIC_MODEL model);

void RAM_setFlag(Memory *ram, unsigned int f, PIC_MODEL model);

void RAM_clearFlag(Memory *ram, unsigned int f, PIC_MODEL model);

void RAM_Write(Memory *ram, size_t addr, unsigned char d, PIC_MODEL model);

unsigned char RAM_Read(Memory *ram, size_t addr, PIC_MODEL model);

#endif // _H_MEMORY_
