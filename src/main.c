/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#define VERSION "0.2"

#include <stdio.h>
#include "asm/picasm.h"
#include "emu/pic.h"
#include "emu/init.h"

void printEEPROM(PIC *p);
PIC *PICEmulate_P16F84A(char *srcName, size_t clk, bool v);
 
int main(int argc, char **argv)
{	
	int filenameIndex = 1;
	bool DoPrintEEPROM = false;
	bool verbose = false;
	bool filePassed = false;
	size_t clockrate = 1;
	
	//Run PIC
	PIC *p;
	
	printf("PIC Emulator v");
	printf(VERSION);
	printf(" by Mahmoud Jaoune\n\n");
	
	if(argc < 2)
	{
		printf("Usage: picemu [options] <source file>\n");
		return 1;
	}
	else
	{
		for(int i = 1; i < argc; i++)
		{
			size_t argLength = strlen(argv[i]);
			
			if(argLength > 0)
			{
				if(argv[i][0] != '-' && !filePassed)
				{
					filenameIndex = i;
					filePassed = true;
				}
				else
				{
					if(!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
					{
						printf("Usage: picemu [options] <source file>\n\n");
						printf("Options:\n"
							   "-h | --help    	   : Prints this screen.\n"
							   "-c | --clockrate   : Select clockrate (In KHz).\n"
							   "--print-eeprom     : Prints EEPROM content after execution.\n"
							   "-v | --verbose     : Executes in verbose mode (Currently does nothing).\n"
							   "-a | --about       : Prints information about this program.\n"
							   "\n");
						
						return 0;
					}
					else if(!strcmp(argv[i], "-c") || !strcmp(argv[i], "--clockrate"))
					{
						if(argc > (i+2))
						{
							clockrate = strtol(argv[i+1], NULL, 10);
							i++;
						}
						else
						{
							printf("Insufficent arguments.\n");
							return 2;
						}
					}
					else if(!strcmp(argv[i], "--print-eeprom"))
					{
						DoPrintEEPROM = true;
					}
					else if(!strcmp(argv[i], "-v") || !strcmp(argv[i], "--verbose"))
					{
						verbose = true;
					}
					else if(!strcmp(argv[i], "-a") || !strcmp(argv[i], "--about"))
					{
						printf("About:\n"
							   "PIC Emulator v"
							   VERSION
							   "\n"
							   "GIT Repo: https://codeberg.org/MJaoune/pic-emulator\n"
							   "\n");
							   
						return 0;
					}
				}
				
			}
		}
	}
	
	
	
	if(filePassed)
	{
		p = PICEmulate_P16F84A(argv[filenameIndex], clockrate, verbose);
		p->state = 2;
		PIC_Run(p);
		
		if(DoPrintEEPROM)
			printEEPROM(p);
	}
	
	return 0;
}

void printEEPROM(PIC *p)
{
	printf("=========================\nEEPROM:\n\n    ");
	for(size_t m = 0; m < 0x10; m++) printf("%02lX ", m);
	printf("\n");
	for(size_t m1 = 0, r = 0; m1 < p->EEPROM->bankCount; m1++)
	{
		for(size_t m2 = 0; m2 < p->EEPROM->size; m2++)
		{
			if(m2 % 16 == 0) printf("\n%02lX: ", r++);
			printf("%02X ", p->EEPROM->data[m1][m2]->data);
		}
	}
	printf("\n=========================\n\n");
}

PIC *PICEmulate_P16F84A(char *srcName, size_t clk, bool v)
{
	unsigned char *out = NULL;
	size_t outSize = ASM_Compile(srcName, NULL, &out);
	
	printf("\nCompilation done, program size: %ld bytes\n\n", outSize);
	
	usleep(3000000);
	
	#ifdef DEBUG
	printf("Program memory:\n-----------\n");
	for(size_t i = 0; i < outSize; i++)
	{
		printf("%02X ", out[i]);
	}
	printf("\n-----------\n\n");
	#endif
	
	//Initialize PIC
	printf("Initializing PIC of model P16F84A...\n\n");
	printf("Clockrate: %ld KHz\n", clk);
	
	usleep(1500000);
	
	Timer *timers = NULL;
	P16F84A_preInit(&timers);
	
	PIC *pic16f84a = PIC_Init(P16F84A, clk, PIC16, 80, 2, 1750, 1, 64, 1, 8, 1);
	pic16f84a->PIC_postInit(pic16f84a->RAM, timers);
	
	printf("Installing program...\n\n");
	
	//Install compiled program
	PIC_installProgram(pic16f84a, out, outSize);
	
	return pic16f84a;
}
