/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "error.h"

void quit(int errorID)
{
	exit(errorID);
}

void throwWarning(unsigned int line, char *fileName, char *warnString)
{
	char warnText[500];
	
	char line_String[10];
	sprintf(line_String, "%d", line);
	
	if(fileName != NULL)
	{
		if(strlen(fileName) > 0)
		{
			strcat(warnText, fileName);
			strcat(warnText, ": Warning at line ");
			strcat(warnText, line_String);
			strcat(warnText, warnString);
		
			printf("%s\n", warnText);
		}
	}
	else 
	{
		strcat(warnText, "Warning: ");
		strcat(warnText, warnString);
		
		printf("Warning: %s\n", warnString);
	}
}

void throwError(int errorID, char *fileName, unsigned int line, char *errorString)
{
	char errorText[500];
	
	char errorID_String[10];
	sprintf(errorID_String, "%d", errorID);
	
	char line_String[10];
	sprintf(line_String, "%d", line);
	
	
	if(fileName != NULL)
	{
		if(strlen(fileName) > 0)
		{
			strcat(errorText, fileName);
			strcat(errorText, ": Error code ");
			strcat(errorText, errorID_String);
			strcat(errorText, " at line ");
			strcat(errorText, line_String);
			strcat(errorText, ": ");
			strcat(errorText, errorString);
			
			printf("%s\n", errorText);
		}
		else 
		{
			strcat(errorText, "Error code ");
			strcat(errorText, errorID_String);
			strcat(errorText, ": ");
			strcat(errorText, errorString);
			
			printf("%s\n", errorText);
		}
	}
	else 
	{
		strcat(errorText, "Error code ");
		strcat(errorText, errorID_String);
		strcat(errorText, ": ");
		strcat(errorText, errorString);
			
		printf("%s\n", errorText);
	}
	
	quit(errorID);
}
