/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "util.h"

bool compareCIS(const char *str1, const char *str2)
{
	size_t length1 = strlen(str1);
	size_t length2 = strlen(str2);
	
	if(length1 != length2) return false;
	
	for(size_t i = 0; i < length1; i++)
	{
		//If char was capital
		if(str1[i] > 0x40 && str1[i] < 0x5B)
		{
			if(!(str1[i] == str2[i] || str1[i] == (str2[i] - 0x20))) return false;
		}
		//If char was small
		else if(str1[i] > 0x60 && str1[i] < 0x7B)
		{
			if(!(str1[i] == str2[i] || str1[i] == (str2[i] + 0x20))) return false;
		}
		//If number or special char
		else if(str1[i] != str2[i]) return false;
	}
	
	return true;
}

bool compareCIS_WL(const char *str1, const char *str2, size_t length)
{	
	for(size_t i = 0; i < length; i++)
	{
		//If char was capital
		if(str1[i] > 0x40 && str1[i] < 0x5B)
		{
			if(!(str1[i] == str2[i] || str1[i] == (str2[i] - 0x20))) return false;
		}
		//If char was small
		else if(str1[i] > 0x60 && str1[i] < 0x7B)
		{
			if(!(str1[i] == str2[i] || str1[i] == (str2[i] + 0x20))) return false;
		}
		//If number or special char
		else if(str1[i] != str2[i]) return false;
	}
	
	return true;
}

bool findInArrayCIS(const char *word, const char **arr, size_t arrSize)
{
	for(size_t i = 0; i < arrSize; i++)
	{
		if(compareCIS(word, arr[i])) return true;
	}
	
	return false;
}

long findInArrayCIS_R(const char *word, const char **arr, size_t arrSize)
{
	for(size_t i = 0; i < arrSize; i++)
	{
		if(compareCIS(word, arr[i])) return i;
	}
	
	return -1;
}

bool findInArrayCS(const char *word, const char **arr, size_t arrSize)
{
	for(size_t i = 0; i < arrSize; i++)
	{
		if(!strcmp(word, arr[i])) return true;
	}
	
	return false;
}

long findInArrayCS_R(const char *word, const char **arr, size_t arrSize)
{
	for(size_t i = 0; i < arrSize; i++)
	{
		if(!strcmp(word, arr[i])) return i;
	}
	
	return -1;
}

bool testEQUSymbolName(const char *label)
{
	unsigned int length = strlen(label);
	for(unsigned int i = 0; i < length; i++)
	{
		//First character must not be a number or a special char.
		if(i == 0)
		{
			if(!((label[i] > 0x40 && label[i] < 0x5B) //Capital letters
			|| (label[i] > 0x60 && label[i] < 0x7B) //Small letters
			|| label[i] == 0x5F)) //Underscore
				return false;
		}
		else
			if(!((label[i] > 0x40 && label[i] < 0x5B) //Capital letters
			|| (label[i] > 0x60 && label[i] < 0x7B) //Small letters
			|| (label[i] >= 0x30 && label[i] <= 0x39) //Numbers
			|| label[i] == 0x5F)) //Underscore
				return false;
	}
	
	return true;
}
