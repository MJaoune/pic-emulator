/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
 
#ifndef _H_PREPROCESSOR_
#define _H_PREPROCESSOR_

#include <stdlib.h>
#include <math.h>
#include "../error.h"

#ifdef PIC_MODEL_16
#include "../parser/pic16/lexer.h"
#endif

#ifdef PIC_MODEL_32
#include "../parser/pic32/lexer.h"
#endif

typedef struct PREPROC_EQUElement {
	LEX_Token *elementName; //Which is the EQU preparameter
	LEX_Token *elementValue; //Which is the EQU parameter
	unsigned int line;	//Line of occurance
} PREPROC_EQUElement;

typedef struct PREPROC_MacroParameter {
	char *label;
	char *replacement;
} PREPROC_MacroParameter;

typedef struct PREPROC_Macro {
	char *label;
	LEX_Token **tokenList;
	unsigned int tokenCount;
	LEX_Token **parameterList;
	unsigned int parameterCount;
} PREPROC_Macro;

void PREPROC_processOperandValues(LEX_Token ***tokenList, unsigned int *tokenCount);

unsigned int PREPROC_countIncludes(LEX_tokenTable tt);

unsigned int PREPROC_countEQUs(LEX_tokenTable tt);

unsigned int PREPROC_countMacros(LEX_tokenTable tt);

unsigned short PREPROC_countMacroParameters(LEX_tokenTable tt, unsigned int i);

bool PREPROC_EQUExists(PREPROC_EQUElement **equList, unsigned int equCount, const char *elementName);

unsigned int PREPROC_processDirectives(LEX_Token ***tokenList, unsigned int *tokenCount, char *sourcePath);

void PREPROC_processIncludes(LEX_Token ***pTokenList, unsigned int *pTokenCount, LEX_Token ***chTokenList, unsigned int *chTokenCount, unsigned int index, char *sourcePath);

void PREPROC_processEQU(LEX_Token ***pTokenList, unsigned int *pTokenCount);

void PREPROC_processMacros(LEX_Token ***pTokenList, unsigned int *pTokenCount);


#endif // _H_PREPROCESSOR
