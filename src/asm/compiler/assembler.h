/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
 
#ifndef _H_ASSEMBLER_
#define _H_ASSEMBLER_

#ifdef PIC_MODEL_16
#include "../parser/pic16/lexer.h"
#endif

#ifdef PIC_MODEL_32
#include "../parser/pic32/lexer.h"
#endif

typedef struct ASM_Instruction
{
	char *label;
	unsigned int opcode;			//14-bit maximum (First two MSB are 0's)
	unsigned short opcodeLength;	//14 - opcodeLength (Bits) = operand length
	unsigned char operandCount;
	unsigned char operandLengths[4];	//Length of each operand (In bits) starting from right (LSB). PIC16 has only max of 2 operands, but the array size is 4 for better scalability.
	LEX_instructionType type;
} ASM_Instruction;

typedef struct ASM_Label
{
	char *label;
	long address;
} ASM_Label;

#define PATTERN_COUNT 6
extern const unsigned int ASM_syntaxPattern[PATTERN_COUNT][2];

#ifdef PIC_MODEL_16
#define INSTRUCTION_COUNT 35
extern const ASM_Instruction ASM_instructionTable[INSTRUCTION_COUNT];  
#endif

void ASM_assemblyCheck(LEX_Token **tokenList, unsigned int tokenCount);
unsigned int ASM_Assemble(LEX_Token **tokenList, unsigned int tokenCount, unsigned char **byteArray);
unsigned int ASM_Walk(LEX_Token **tokenList, unsigned int tokenCount, unsigned char **byteArray, ASM_Label **labelTable, unsigned int tableSize, bool pretend, bool fillLabelTable);
int ASM_getInstrInTable(const char *instruction);
unsigned char *ASM_calculateOperandSizes(ASM_Instruction inst);
long ASM_searchLabelTable(const char *label, ASM_Label *labelTable, unsigned int entryCount);
char *ASM_getTokenTypes(unsigned int tt);

#endif // _H_ASSEMBLER_
