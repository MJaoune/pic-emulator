/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "assembler.h"

#ifdef PIC_MODEL_16
const unsigned int ASM_syntaxPattern[][2] =
{
	{DIRECTIVE, DIRECTIVE_PARAMETER | ENDL}, //ENDL such as the case of "end" directive
	{DIRECTIVE_PREPARAMETER, DIRECTIVE},
	{LABEL, INSTRUCTION | ENDL},
	{INSTRUCTION, OPERAND | ENDL},	//ENDL such as "SLEEP", "NOP"
	{OPERAND, COMMA | ENDL},
	{COMMA, OPERAND}
};

/*typedef struct ASM_Instruction
{
	char *label;
	unsigned int opcode;			//14-bit maximum (First two MSB are 0's)
	unsigned short opcodeLength;	//14 - opcodeLength (Bits) = operand length
	unsigned char operandCount;
	unsigned char operandLengths[4];	//Length of each operand (In bits) starting from right (LSB). PIC16 has only max of 2 operands, but the array size is 4 for better scalability.
	LEX_instructionType type;
} ASM_Instruction;*/

/*const ASM_Instruction ASM_instructionTable[] = 
{
	//FD
	{"ADDWF", 0x7, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"ANDWF", 0x5, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"COMF", 0x9, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"DECF", 0x3, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"DECFSZ", 0xB, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"INCF", 0xA, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"INCFSZ", 0xF, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"MOVF", 0x8, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"RLF", 0xD, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"RRF", 0xC, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"SUBWF", 0x2, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"SWAPF", 0xE, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"XORWF", 0x6, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	
	//NOP
	{"CLRW", 0x2, 7, 0, {0, 0, 0 ,0}, NOP_INSTRUCTION},
	{"NOP", 0x0, 14, 0, {0, 0, 0 ,0}, NOP_INSTRUCTION},
	{"CLRWDT", 0x64, 14, 0, {0, 0, 0 ,0}, NOP_INSTRUCTION},
	{"RETFIE", 0x9, 14, 0, {0, 0, 0 ,0}, NOP_INSTRUCTION},
	{"RETURN", 0x8, 14, 0, {0, 0, 0 ,0}, NOP_INSTRUCTION},
	{"SLEEP", 0x63, 14, 0, {0, 0, 0 ,0}, NOP_INSTRUCTION},
	
	//F
	{"CLRF", 0x3, 7, 1, {7, 0, 0 ,0}, F_INSTRUCTION},
	{"MOVWF", 0x1, 7, 1, {7, 0, 0 ,0}, F_INSTRUCTION},
	
	//FB
	{"BCF", 0x4, 4, 2, {7, 3, 0 ,0}, FB_INSTRUCTION},
	{"BSF", 0x5, 4, 2, {7, 3, 0 ,0}, FB_INSTRUCTION},
	{"BTFSC", 0x6, 4, 2, {7, 3, 0 ,0}, FB_INSTRUCTION},
	{"BTFSS", 0x7, 4, 2, {7, 3, 0 ,0}, FB_INSTRUCTION},
	
	//K
	{"ADDLW", 0x1F, 5, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
	{"ANDLW", 0x39, 6, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
	{"CALL", 0x4, 3, 1, {11, 0, 0 ,0}, K_INSTRUCTION},
	{"GOTO", 0x5, 3, 1, {11, 0, 0 ,0}, K_INSTRUCTION},
	{"IORLW", 0x38, 6, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
	{"MOVLW", 0xC, 6, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
	{"RETLW", 0xD, 6, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
	{"SUBLW", 0x1E, 5, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
	{"XORLW", 0x3A, 6, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
};*/

const ASM_Instruction ASM_instructionTable[] = 
{
	//FD
	{"ADDWF", 0x700, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"ANDWF", 0x500, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"COMF", 0x900, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"DECF", 0x300, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"DECFSZ", 0xB00, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"INCF", 0xA00, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"INCFSZ", 0xF00, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"IORWF", 0x400, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"MOVF", 0x800, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"RLF", 0xD00, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"RRF", 0xC00, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"SUBWF", 0x200, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"SWAPF", 0xE00, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	{"XORWF", 0x600, 6, 2, {7, 1, 0 ,0}, FD_INSTRUCTION},
	
	//NOP
	{"CLRW", 0x100, 7, 0, {0, 0, 0 ,0}, NOP_INSTRUCTION},
	{"NOP", 0x0, 14, 0, {0, 0, 0 ,0}, NOP_INSTRUCTION},
	{"CLRWDT", 0x64, 14, 0, {0, 0, 0 ,0}, NOP_INSTRUCTION},
	{"RETFIE", 0x9, 14, 0, {0, 0, 0 ,0}, NOP_INSTRUCTION},
	{"RETURN", 0x8, 14, 0, {0, 0, 0 ,0}, NOP_INSTRUCTION},
	{"SLEEP", 0x63, 14, 0, {0, 0, 0 ,0}, NOP_INSTRUCTION},
	
	//F
	{"CLRF", 0x180, 7, 1, {7, 0, 0 ,0}, F_INSTRUCTION},
	{"MOVWF", 0x80, 7, 1, {7, 0, 0 ,0}, F_INSTRUCTION},
	
	//FB
	{"BCF", 0x1000, 4, 2, {7, 3, 0 ,0}, FB_INSTRUCTION},
	{"BSF", 0x1400, 4, 2, {7, 3, 0 ,0}, FB_INSTRUCTION},
	{"BTFSC", 0x1800, 4, 2, {7, 3, 0 ,0}, FB_INSTRUCTION},
	{"BTFSS", 0x1C00, 4, 2, {7, 3, 0 ,0}, FB_INSTRUCTION},
	
	//K
	{"ADDLW", 0x3E00, 5, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
	{"ANDLW", 0x3900, 6, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
	{"CALL", 0x2000, 3, 1, {11, 0, 0 ,0}, K_INSTRUCTION},
	{"GOTO", 0x2800, 3, 1, {11, 0, 0 ,0}, K_INSTRUCTION},
	{"IORLW", 0x3800, 6, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
	{"MOVLW", 0x3000, 6, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
	{"RETLW", 0x3400, 6, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
	{"SUBLW", 0x3C00, 5, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
	{"XORLW", 0x3A00, 6, 1, {8, 0, 0 ,0}, K_INSTRUCTION},
};
#endif

///Checks syntax pattern according to ASM_syntaxPattern[] structure.
void ASM_assemblyCheck(LEX_Token **tokenList, unsigned int tokenCount)
{
	for(unsigned int i = 0; i < tokenCount - 1; i++)
	{
		LEX_tokenType tt = tokenList[i]->type;
		LEX_tokenType nextTT = tokenList[i+1]->type;
		
		for(unsigned int j = 0; j < PATTERN_COUNT; j++)
		{
			if(tt == ASM_syntaxPattern[j][0])
			{
				if(!(nextTT & ASM_syntaxPattern[j][1]))
				{
					char *err = (char *)malloc(100);
					
					LEX_tokenTypeToString(*(tokenList[i]), &err);
					
					strcat(err, " '");
					
					strcat(err, tokenList[i]->label);
					
					strcat(err, "' ");
					
					strcat(err, "expects one of the following token type(s) after it: ");
					
					strcat(err, ASM_getTokenTypes(ASM_syntaxPattern[j][1]));
					
					throwError(70, tokenList[i]->filename, tokenList[i]->lineNumber, err);
				}
			}
		}
	}
}

///Assembles tokenlist and places binary output in the byteArray which has its size dynamically allocated along the process. Returns size of byteArray
unsigned int ASM_Assemble(LEX_Token **tokenList, unsigned int tokenCount, unsigned char **byteArray)
{
	//Check for assembling errors
	ASM_assemblyCheck(tokenList, tokenCount);
	
	#ifdef DEBUG_L2
	printf("Started filling labelTable...\n");
	#endif
	
	//First we need to translate each label to an address according to its position, and we put them in a table for later reference.
	ASM_Label *labelTable;
	unsigned int tableSize = ASM_Walk(tokenList, tokenCount, byteArray, &labelTable, 0, true, true);
	
	#ifdef DEBUG_L2
	printf("Started compilation...\n");
	#endif
	
	//Now we compile
	unsigned int outputSize = ASM_Walk(tokenList, tokenCount, byteArray, &labelTable, tableSize, false, false);
	
	return outputSize;
}

///Walks on each instruction and compiles it (If pretend is false). Returns size of compilation output.
///If fillLabelTable is true, size of the labelTable is returned instead.
///All sizes that do not divide by 8 are ceiled. So a 14-bit instruction becomes 16-bit (As in the case of NOP). If operand is of size 12-bit, it becomes 16-bit (As in the case of GOTO).
unsigned int ASM_Walk(LEX_Token **tokenList, unsigned int tokenCount, unsigned char **byteArray, ASM_Label **labelTable, unsigned int tableSize, bool pretend, bool fillLabelTable)
{
	unsigned int bufferSize = 0; //Size of byteArray (Currently)
	if(fillLabelTable) tableSize = 0;	//Size of labelTable (0 if in table filling phase, other than that, must be specified)
	
	long ip = 0;
	bool firstFill = false;
	
	for(unsigned int i = 0; i < tokenCount;)
	{
		#ifdef DEBUG_L2
		printf("%d.\n", i);
		printf("Processing token %s...\n", tokenList[i]->label);
		#endif
		
		if(LEX_isInstruction(tokenList[i]->label))
		{
			int index = ASM_getInstrInTable(tokenList[i]->label);
			if(index == -1) continue; //It would be funny if this was ever reached
			
			//Setup instruction
			const ASM_Instruction *inst = &(ASM_instructionTable[index]);
			
			unsigned short fullInstruction = 0; //14-bit instruction
			
			unsigned short inst_opcodeSize = inst->opcodeLength;
			
			#ifdef DEBUG_L2
			printf("opcode size %d\n", inst_opcodeSize);
			#endif
			
			//unsigned short *inst_opcode = (unsigned short *)malloc(inst_opcodeSize);
			unsigned short inst_opcode = inst->opcode;
			
			//for(unsigned short s = 0; s < inst_opcodeSize; s++)
			//	inst_opcode[s] = (inst->opcode >> (8 * s)) & 0xFF;
			
			unsigned char inst_operandCount = inst->operandCount;
			unsigned char *inst_operandSizes = ASM_calculateOperandSizes(*inst);
			unsigned char inst_operandSizesTotal = inst_operandSizes[0] + inst_operandSizes[1] + inst_operandSizes[2] + inst_operandSizes[3];
			
			//Read operands
			unsigned char operandCount = 0;
			
			//long *operands = (long *)malloc(sizeof(long) * inst_operandCount);
			
			unsigned short operands = 0;
			
			#ifdef DEBUG_L2
			printf("Reading operands...\n");
			#endif
			
			unsigned int iNext = 1;	//We skip instruction
			unsigned char operandTotalSize = 0;
			for(unsigned int j = i+1, c = 0; tokenList[j]->type != ENDL && j < tokenCount; j++, iNext++)
			{	
				if(tokenList[j]->type == COMMA) continue;
				
				if(operandCount > inst_operandCount)
					throwError(52, tokenList[j]->filename, tokenList[j]->lineNumber, "Too many operands specified.");
				
				if(tokenList[j]->isValue) 
				{
					#ifdef DEBUG_L2
					printf("Storing operand of value %ld\n", *(tokenList[j]->value));
					#endif

					operands |= (unsigned short)(*(tokenList[j]->value))  << operandTotalSize;

					operandTotalSize += inst_operandSizes[c++];
					operandCount++;
					
				}
				//If operand has no numerical value, it might be relying on a label, but this is only if it requires an address
				else if(LEX_requiresAddress(*(tokenList[i])))
				{
					#ifdef DEBUG_L2
					printf("Label operand processing...\n");
					#endif
					
					long operandAddr = ASM_searchLabelTable(tokenList[j]->label, *labelTable, tableSize);
					if(operandAddr == -1 && !fillLabelTable) throwError(57, tokenList[j]->filename, tokenList[j]->lineNumber, "Label specified as an operand was not found.");
					

					operands |= (unsigned short)((*labelTable)[operandAddr].address) << operandTotalSize; //Store address instead of label

					operandTotalSize += inst_operandSizes[c++];
					operandCount++;
				}
				else
					throwError(51, tokenList[j]->filename, tokenList[j]->lineNumber, "Expected numerical operand.");
			}
			
			iNext++; //Skip ENDL
			
			if(operandCount < inst_operandCount && inst->type != FD_INSTRUCTION)
				throwError(53, tokenList[i]->filename, tokenList[i]->lineNumber, "Insufficent operands specified.");
			
			if(inst->type == FD_INSTRUCTION)
			{
				if(operandCount == inst_operandCount - 1)
				{
					//This is if user forgot to add "d" operand in FD instr such as MOVF
					//We add 0 by default
					#ifdef DEBUG_L2
					printf("FD instruction has no 'd'. Assuming 0\n");
					#endif
					operands &= ((1 << inst_operandSizesTotal) ^ 0xFF);
					operandTotalSize++; //1-bit only
				}
				else if(operandCount < inst_operandCount - 1)
					throwError(53, tokenList[i]->filename, tokenList[i]->lineNumber, "Insufficent operands specified.");
			}
			
			//We now have full instruction
			#ifdef DEBUG_L2
			printf("Opcode: %d\nOperands: %d\n", inst_opcode, operands);
			#endif
			
			fullInstruction = inst_opcode | operands;
			
			//Start assembling
			
			//Write opcode and operands
			bufferSize += 2;
			
			if(!pretend && !fillLabelTable)
			{
				if(!firstFill) 
				{
					*byteArray = (unsigned char *)calloc(bufferSize, sizeof(unsigned char));
					firstFill = true;
				}
				else
					*byteArray = (unsigned char *)realloc(*byteArray, bufferSize);
					
				(*byteArray)[ip++] = (fullInstruction >> 8) & 0xFF;
				(*byteArray)[ip++] = fullInstruction & 0xFF;
			}
			else ip += 2;
			
			/*for(unsigned short s = 0; s < inst_opcodeSize; s++)
			{
				
				if(!pretend && !fillLabelTable)
					(*byteArray)[ip] = inst_opcode[s];
				ip++;
			}*/
				
			//Write operands
			/*#ifdef DEBUG_L2
			printf("Writing operands...\n");
			#endif
			for(unsigned char s = 0; s < inst_operandCount; s++)
			{
				bufferSize += inst_operandSizes[s];
				if(!pretend && !fillLabelTable)
				{
					if(!firstFill) 
					{
						*byteArray = (unsigned char *)calloc(bufferSize, sizeof(unsigned char));
						firstFill = true;
					}
					else
						*byteArray = (unsigned char *)realloc(*byteArray, bufferSize);
				}
				
				for(unsigned char s2 = 0; s2 < inst_operandSizes[s]; s2++)
				{
					if(!pretend && !fillLabelTable)
						(*byteArray)[ip] = (operands[s] >> (8 * s2)) & 0xFF;
					ip++;
				}
			}*/
			
			i += iNext;
		}
		else if(tokenList[i]->type == DIRECTIVE)
		{
			if(compareCIS(tokenList[i]->label, "ORG"))
			{
				//ORG shall set IP position
				if(i+1 < tokenCount)
				{
					if(tokenList[i+1]->type == DIRECTIVE_PARAMETER)
					{
						long oldIP = ip;
						if(tokenList[i+1]->isValue)
						{
							long newPos = *(tokenList[i+1]->value);
							ip = newPos;
						}
						//If operand has no numerical value, it might be relying on a label
						else
						{
							long operandAddr = ASM_searchLabelTable(tokenList[i+1]->label, *labelTable, tableSize);
							
							if(operandAddr == -1 && !fillLabelTable) throwError(58, tokenList[i+1]->filename, tokenList[i+1]->lineNumber, "Label specified as a directive for ORG was not found.");
							
							ip = (*labelTable)[operandAddr].address; //Store address of label
						}
						
						if(ip > bufferSize) 
						{
							if(!fillLabelTable)
							{
								if(!firstFill) 
								{
									*byteArray = (unsigned char *)calloc(ip, sizeof(unsigned char));
									firstFill = true;
								}
								else
								{
									*byteArray = (unsigned char *)realloc(*byteArray, ip);
									
									//Initialize to 0
									for(unsigned int c = oldIP; c < ip; c++)
										(*byteArray)[c] = 0;
								}
								
								bufferSize = ip;
							}
						}
					}
					else 
						throwError(55, tokenList[i]->filename, tokenList[i]->lineNumber, "Expected a parameter for directive ORG.");
				}
			}
			else if(compareCIS(tokenList[i]->label, "END"))
			{
				//Stop compilation
				break;
			}
			
			i += 2;	//ORG and its parameter
		}
		else
		{
			//Fill table if label found and currently in fillLabelTable phase
			if(tokenList[i]->type == LABEL)
			{
				if(fillLabelTable)
				{
					if(ASM_searchLabelTable(tokenList[i]->label, *labelTable, tableSize) != -1)
						throwError(56, tokenList[i]->filename, tokenList[i]->lineNumber, "Label was used before.");
					
					if(!tableSize)
					{
						#ifdef DEBUG_L2
						printf("Allocating table first time...\n");
						#endif
						*labelTable = (ASM_Label *)malloc(sizeof(ASM_Label));
						tableSize++;
					}
					else	
					{
						#ifdef DEBUG_L2
						printf("Reallocating table...\n");
						#endif
						*labelTable = (ASM_Label *)realloc(*labelTable, ++tableSize * sizeof(ASM_Label));
					}
					
					#ifdef DEBUG_L2
					printf("Filling table...\n");
					#endif
					(*labelTable)[tableSize-1].label = tokenList[i]->label;
					(*labelTable)[tableSize-1].address = ip;
					
					printf("Label %s was given address %d\n", tokenList[i]->label, ip);
				}
			}
			
			i++;
		}
	}
	
	if(!fillLabelTable) 
		return bufferSize; 
	else 
		return tableSize;
}

///Returns position of instruction in instruction set table (-1 returned if not found)
int ASM_getInstrInTable(const char *instruction)
{
	for(unsigned int i = 0; i < INSTRUCTION_COUNT; i++)
		if(compareCIS(instruction, ASM_instructionTable[i].label))
			return i;
			
	return -1;
}

///Returns an array of the suitable size of each operand.
unsigned char *ASM_calculateOperandSizes(ASM_Instruction inst)
{
	unsigned char operandCount = inst.operandCount;
	
	unsigned char *sizes = (unsigned char *)malloc(operandCount);
	
	for(unsigned char i = 0; i < operandCount; i++)
		sizes[i] = inst.operandLengths[i];
	
	return sizes;
}

///Searches for specified entry in table.
long ASM_searchLabelTable(const char *label, ASM_Label *labelTable, unsigned int entryCount)
{
	for(unsigned int i = 0; i < entryCount; i++)
	{
		if(!strcmp(label, labelTable[i].label)) 
			return i;
	}
	
	return -1;
}

///This function returns the token types compatible with the passed LEX_tokenType structure.
///Returns a string of token types separated by a comma.
char *ASM_getTokenTypes(unsigned int tt)
{
	char *tokens = (char *)calloc(150, sizeof(char));
	
	if(tt & DIRECTIVE)
		strcat(tokens, " a directive, ");
	
	if(tt & DIRECTIVE_PREPARAMETER)
		strcat(tokens, " a directive preparameter, ");
	
	if(tt & DIRECTIVE_PARAMETER)
		strcat(tokens, " a directive parameter, ");
	
	if(tt & LABEL)
		strcat(tokens, " a label, ");
	
	if(tt & INSTRUCTION)
		strcat(tokens, " an instruction, ");
	
	if(tt & OPERAND)
		strcat(tokens, " an operand, ");
	
	if(tt & ENDL)
		strcat(tokens, " a newline, ");
	
	if(tt & COMMA)
		strcat(tokens, " a comma, ");
	
	//Remove last comma
	tokens[strlen(tokens) - 2] = '.';
	tokens[strlen(tokens) - 1] = '\0';
	
	return tokens;
}
