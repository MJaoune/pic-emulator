/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
 
#include "preprocessor.h"

///Does everything needed before starting assembling process
unsigned int PREPROC_processDirectives(LEX_Token ***tokenList, unsigned int *tokenCount, char *sourcePath)
{
	//We need to process #includes alone first.
	#ifdef DEBUG_L2
	printf("Preprocessing...");
	#endif
	
	//Order of following function calls is IMPORTANT.
	PREPROC_processIncludes(NULL, NULL, tokenList, tokenCount, 0, sourcePath);
	PREPROC_processMacros(tokenList, tokenCount);
	PREPROC_processOperandValues(tokenList, tokenCount);
	PREPROC_processEQU(tokenList, tokenCount);
	
	return 0;
	
}

///Fixes values (And does numerical conversion if needed) for operands and directive parameters.
///In brief, it just converts ASCII to real numerical values.
void PREPROC_processOperandValues(LEX_Token ***tokenList, unsigned int *tokenCount)
{
	for(unsigned int i = 0; i < *tokenCount; i++)
	{
		LEX_Token *p = (*tokenList)[i];
		long buffer = 0; //Contains converted value
		
		//Mode 0: Raw decimal value, no numerical conversion needed nor parsing.
		//Mode 1: Decimal value, no numerical conversion needed (Differs from mode 0 in that it requires parsing).
		//Mode 2: Hexadecimal value, numerical conversion needed (Symbol form)
		//Mode 3: Hexadecimal value, numerical conversion needed (0x form)
		//Mode 4: Octal value, numerical conversion needed.
		//Mode 5: Binary value, numerical conversion needed.
		unsigned char mode = 0;
		
		//We don't want to modify the real content
		char *labelCopy = (char *)malloc(p->labelLength - 1);
		unsigned int *labelCopyLength = (unsigned int *)malloc(sizeof(unsigned int));
		*labelCopyLength = p->labelLength - 1; //Don't copy null terminator
		for(unsigned int c = 0; c < *labelCopyLength; c++) labelCopy[c] = p->label[c];
		
		//Only directive parameters and operands must be numerical
		if(p->type == DIRECTIVE_PARAMETER || p->type == OPERAND)
		{	
			unsigned char parsingCheck = 0; //Parsing check is done only once, even if cursor is reset.
			unsigned char negativeCheck = 0; //Sign check is done once too.
			bool isLabel = false; //Not every operand or directive parameter is a numerical value, can be a label too.
			
			unsigned short sign; //Contains sign of value
			
			for(unsigned int j = 0; j < *labelCopyLength; j++)
			{
				//Deal with negative sign
				if(!negativeCheck++)
				{
					if(labelCopy[j] == '-' && j == 0)
					{
						sign = 2;	//Negative value
						
						//Remove the negative sign
						for(unsigned int k = 1, l = 0; k < *labelCopyLength; k++, l++) labelCopy[l] = labelCopy[k];
						*labelCopyLength -= 1;
						
						//The two lines below are to reset cursor after removing the negative sign
						j = -1;
						continue;
					} 
					else sign = 1; //Positive value
				}
				
				if(!parsingCheck++)
				{
					if(j == 0)
					{
						//ASCII value (Do nothing, just remove quotation marks)
						if(labelCopy[j] == '\'')
						{
							if(!(labelCopy[*labelCopyLength - 1] == '\''))
								throwError(10, p->filename, p->lineNumber, "ASCII value not enclosed.");
							else 
							{
								//Remove the quotation mark
								for(unsigned int k = j+1, l = 0; k < *labelCopyLength - 1; k++, l++) labelCopy[l] = labelCopy[k];
								*labelCopyLength -= 2;
								break;
							}
						}
						//The A'some text' case
						if(labelCopy[j] == 'A' && *labelCopyLength > 1)
						{
							if(labelCopy[j+1] == '\'')
							{
								if(!(labelCopy[*labelCopyLength - 1] == '\'' && labelCopy[j+1] == '\''))
									throwError(10, p->filename, p->lineNumber, "ASCII value not enclosed.");
								else 
								{
									//Remove the quotation mark and the 'A' symbol
									for(unsigned int k = j+2, l = 0; k < *labelCopyLength - 1; k++, l++) labelCopy[l] = labelCopy[k];
									*labelCopyLength -= 3;
									break;
								}
							}
						}
						
						switch(labelCopy[j])
						{
							case 'D':
								if(*labelCopyLength > 1)
									if(labelCopy[j+1] == '\'')
										mode = 1;
								break;
							case 'H':
								if(*labelCopyLength > 1)
									if(labelCopy[j+1] == '\'')
										mode = 2;
								break;
							case '0':
								if(*labelCopyLength > 1)
									if(labelCopy[j+1] == 'x')
										mode = 3;
								break;
							case 'O':
								if(*labelCopyLength > 1)
									if(labelCopy[j+1] == '\'')
										mode = 4;
								break;
							case 'B':
								if(*labelCopyLength > 1)
									if(labelCopy[j+1] == '\'')
										mode = 5;
								break;
							default:
								break;
						}
						
						if(mode >= 1 && mode <= 5 && mode != 3)
						{
							if(!(labelCopy[*labelCopyLength - 1] == '\'' && labelCopy[j+1] == '\''))
								throwError(10, p->filename, p->lineNumber, "Value not properly enclosed.");
							else 
							{
								//Remove the quotation mark and the numerical type symbol
								for(unsigned int k = j+2, l = 0; k < *labelCopyLength - 1; k++, l++) labelCopy[l] = labelCopy[k];
								*labelCopyLength -= 3;
								
								//The two lines below are to reset cursor after removing quotation
								j = -1;
								continue;
							}
						}
						
						if(mode == 3)
						{
							
							//Remove hexadecimal prefix (That is '0x')
							for(unsigned int k = j+2, l = 0; k < *labelCopyLength; k++, l++) labelCopy[l] = labelCopy[k];
							*labelCopyLength -= 2;
							
							//The two lines below are to reset cursor after removing prefix
							j = -1;
							continue;
						}
					}
				}
				//Directive parameter or operand appears to be non-numerical, leave it alone.
				else if(!((labelCopy[j] >= 0x30 && labelCopy[j] <= 0x39) || (labelCopy[j] >= 0x41 && labelCopy[j] <= 0x46) || (labelCopy[j] >= 0x61 && labelCopy[j] <= 0x66))) 
				{
					isLabel = true;
					break;
				}
				
				//ASCII conversion (Alphabets (A-F) will remain for Hexadecimal case)
				if(labelCopy[j] >= 0x30 && labelCopy[j] <= 0x39)
					labelCopy[j] -= 0x30;
			}
			
			if(isLabel) continue; //Skip the non-numerical value
			
			//The below check is just to prevent memory overflow since we will be using "long" to store the converted values.
			if(*labelCopyLength > 10) throwError(13, p->filename, p->lineNumber, "Value too long.");
			
			//Numerical conversion begins here
			//Decimal
			if(mode == 0 || mode == 1)
			{
				for(unsigned int j = 0, k = *labelCopyLength - 1; j < *labelCopyLength; j++, k--)
				{
					//if(!(labelCopy[j] >= 0 && labelCopy[j] <= 9)) throwError(14, p->lineNumber, "Bad decimal value.");
					buffer += (int)pow(10, k)*labelCopy[j];
				}
				
				
				p->isValue = true;
				
				//Copy converted value to token
				p->value = (long *)malloc(sizeof(long));
				*(p->value) = buffer;
				
				//Set its sign
				p->valueType = (unsigned short *)malloc(sizeof(unsigned short));
				*(p->valueType) = sign;
			}
			//Hexadecimal
			else if(mode == 2 || mode == 3)
			{
				for(unsigned int j = 0, k = *labelCopyLength - 1; j < *labelCopyLength; j++, k--)
				{
					//if(!((labelCopy[j] >= 0 && labelCopy[j] <= 9) || (labelCopy[j] >= 0x41 && labelCopy[j] <= 0x46) || (labelCopy[j] >= 0x61 && labelCopy[j] <= 0x66))) throwError(14, p->lineNumber, "Bad hexadecimal value.");
					
					if(labelCopy[j] >= 0 && labelCopy[j] <= 9)
					{
						buffer += (int)pow(16, k)*labelCopy[j];
					}
					else
					{
						switch(labelCopy[j])
						{
							case 'A':
							case 'a':
								buffer += (int)pow(16, k)*10;
								break;
							case 'B':
							case 'b':
								buffer += (int)pow(16, k)*11;
								break;
							case 'C':
							case 'c':
								buffer += (int)pow(16, k)*12;
								break;
							case 'D':
							case 'd':
								buffer += (int)pow(16, k)*13;
								break;
							case 'E':
							case 'e':
								buffer += (int)pow(16, k)*14;
								break;
							case 'F':
							case 'f':
								buffer += (int)pow(16, k)*15;
								break;
						}
					}
					
					p->isValue = true;
					
					//Copy converted value to token
					p->value = (long *)malloc(sizeof(long));
					*(p->value) = buffer;
					
					//Set its sign
					p->valueType = (unsigned short *)malloc(sizeof(unsigned short));
					*(p->valueType) = sign;
				}
			}
			//Octal
			else if(mode == 4)
			{
				for(unsigned int j = 0, k = *labelCopyLength - 1; j < *labelCopyLength; j++, k--)
				{
					buffer += (int)pow(8, k)*labelCopy[j];
				}
				
				p->isValue = true;
				
				//Copy converted value to token
				p->value = (long *)malloc(sizeof(long));
				*(p->value) = buffer;
				
				//Set its sign
				p->valueType = (unsigned short *)malloc(sizeof(unsigned short));
				*(p->valueType) = sign;
			}
			//Binary
			else if(mode == 5)
			{
				for(unsigned int j = 0, k = *labelCopyLength - 1; j < *labelCopyLength; j++, k--)
				{
					buffer += (int)pow(2, k)*labelCopy[j];
				}
				
				p->isValue = true;
				
				//Copy converted value to token
				p->value = (long *)malloc(sizeof(long));
				*(p->value) = buffer;
				
				//Set its sign
				p->valueType = (unsigned short *)malloc(sizeof(unsigned short));
				*(p->valueType) = sign;
			}
			
			//In case it was a negative value
			if(p->valueType != NULL) 
				if(*(p->valueType) == 2) 
					*(p->value) *= -1;
			
			#ifdef DEBUG_L2
			if(p->value != NULL) printf("Converted numerical value: %ld\n", *(p->value));
			#endif
		}
		
		free(labelCopy);
		free(labelCopyLength);
	}
}

///Counts number of #includes in the source
unsigned int PREPROC_countIncludes(LEX_tokenTable tt)
{
	unsigned int includeCount = 0;
	
	for(unsigned int i = 0; i < tt.tokenCount; i++)
	{
		LEX_Token *t = tt.tokenList[i];
		if(compareCIS(t->label, "#include") && t->type == DIRECTIVE)
			includeCount++;
	}
		
	return includeCount;
}

///Counts number of EQUs in the source
unsigned int PREPROC_countEQUs(LEX_tokenTable tt)
{
	unsigned int EQUCount = 0;
	
	for(unsigned int i = 0; i < tt.tokenCount; i++)
	{
		LEX_Token *t = tt.tokenList[i];
		if(compareCIS(t->label, "EQU") && t->type == DIRECTIVE)
			EQUCount++;
	}
		
	return EQUCount;
}

///Counts number of macros (Approx.)
unsigned int PREPROC_countMacros(LEX_tokenTable tt)
{
	unsigned int macroCount = 0;
	for(unsigned int i = 0; i < tt.tokenCount; i++)
	{
		LEX_Token *t = tt.tokenList[i];
		if(compareCIS(t->label, "macro") && t->type == DIRECTIVE)
			macroCount++;
	}
	return macroCount;
}

///Counts number of parameters a macro has
///i: Index of first parameter
unsigned short PREPROC_countMacroParameters(LEX_tokenTable tt, unsigned int i)
{
	unsigned short count = 0;
	
	for(unsigned short j = i; j < tt.tokenCount && tt.tokenList[j]->type == DIRECTIVE_PARAMETER && tt.tokenList[j]->type != ENDL; j++, count++);
	
	return count;
}

///Checks if EQU already exists
bool PREPROC_EQUExists(PREPROC_EQUElement **equList, unsigned int equCount, const char *elementName)
{
	for(unsigned int i = 0; i < equCount; i++)
	{
		if(!strcmp(equList[i]->elementName->label, elementName))
			return true;
	}
	
	return false;
}

///[Recursive function] It keeps accessing the included files until one with no more includes is reached.
///When an included file with no more includes is found, its tokenList (Child) is merged with parent tokenList.
void PREPROC_processIncludes(LEX_Token ***pTokenList, unsigned int *pTokenCount, LEX_Token ***chTokenList, unsigned int *chTokenCount, unsigned int index, char *sourcePath)
{	
	for(unsigned int i = 0; i < *chTokenCount; i++)
	{	
		if(compareCIS((*chTokenList)[i]->label, "#include"))
		{
			if(*chTokenCount > 1)
			{
				if((*chTokenList)[i+1]->type == DIRECTIVE_PARAMETER)
				{
					bool isLocal = (*chTokenList)[i+1]->label[0] == '"';
					if(isLocal || (*chTokenList)[i+1]->label[0] == '<')
					{
						//Calculate filename length
						unsigned int fileNameLength = 0;
						for(unsigned int j = 1; (*chTokenList)[i+1]->label[j] != '"' && (*chTokenList)[i+1]->label[j] != '>'; j++, fileNameLength++);
						
						//Get filename
						char *fileName = (char *)malloc(fileNameLength + 1);
						for(unsigned int j = 1; j < fileNameLength + 1; j++) fileName[j-1] = (*chTokenList)[i+1]->label[j];
						fileName[fileNameLength] = '\0';
						
						
						//Here we need to take into account local/global file including
						/*
						 * TODO
						 */
						 
						//If there is a sourcePath, cat it.
						char *fullPath;
						size_t fullPathLength;
						if(sourcePath != NULL)
						{
							fullPathLength = strlen(sourcePath) + strlen(fileName) + 1;
							fullPath = (char *)malloc(fullPathLength);
							
							for(size_t c = 0; c < strlen(sourcePath); c++)
								fullPath[c] = sourcePath[c];
							
							strcat(fullPath, fileName);
							fullPath[fullPathLength] = '\0';
						} 
						else
						{
							fullPathLength = fileNameLength;
							fullPath = fileName;
						}
						
						#ifdef DEBUG_L2
						printf("Opening included file %s\n", fullPath);
						#endif
						
						
						//Check if file exists
						FILE *fTest = fopen(fullPath, "r");
						if(fTest == NULL)
						{
							char *errorString = (char *)malloc(fullPathLength + 20);
							strcpy(errorString, fullPath);
							strcat(errorString, " not found!");
								
							throwError(4, (*chTokenList)[i]->filename, (*chTokenList)[i]->lineNumber, errorString);
							free(fileName);	//Probably will never be reached, just a routine :)
							if(sourcePath != NULL) free(fullPath);
						}
						fclose(fTest);
						
						#ifdef DEBUG_L2
						printf("File %s opened\n", fullPath);
						#endif
						
						FILE *iFile = fopen(fullPath, "r");

						LEX_tokenTable src = LEX_tokenizeFile(iFile, fullPath);
						PREPROC_processIncludes(chTokenList, chTokenCount, &src.tokenList, &src.tokenCount, i, sourcePath);
						
						free(fileName);
						if(sourcePath != NULL) free(fullPath);
					}
					else throwError(3, (*chTokenList)[i]->filename, (*chTokenList)[i]->lineNumber, "Included file name must be embedded with either \" or double triangle brackets.");
				}
				else throwError(2, (*chTokenList)[i]->filename, (*chTokenList)[i]->lineNumber, "Expected parameter after directive \"#include\".");
			}
			else throwError(2, (*chTokenList)[i]->filename, (*chTokenList)[i]->lineNumber, "Expected parameter after directive \"#include\".");
		}
	}
	
	
	//This check is crucial to prevent memory access voilation for the first tokenList.
	//This won't be called unless the file is an 'included' file only, not the main one.
	if(pTokenCount != NULL) 
	{
		//Remove "#include" and its directive (And the newline after it)
		LEX_removeTokenAt(pTokenList, pTokenCount, index);
		LEX_removeTokenAt(pTokenList, pTokenCount, index);
		LEX_removeTokenAt(pTokenList, pTokenCount, index);
			
		#ifdef DEBUG_L2
		printf("Removed #include line.\n");
		#endif
			
		//Merge files
		LEX_Concatenate(pTokenList, pTokenCount, *chTokenList, *chTokenCount, index);
		return;
	}
}

///When an EQU is found, all later occurances of the symbol defined will be replaced by the set value.
///Note: Only first EQU is processed, duplicates are ignored.
void PREPROC_processEQU(LEX_Token ***pTokenList, unsigned int *pTokenCount)
{	
	for(unsigned int i = 0; i < *pTokenCount; i++)
	{
		if(compareCIS((*pTokenList)[i]->label, "EQU") && (*pTokenList)[i]->type == DIRECTIVE)
		{
			if(i > 0)
			{
				if((*pTokenList)[i-1]->type != DIRECTIVE_PREPARAMETER)
					throwError(21, (*pTokenList)[i]->filename, (*pTokenList)[i]->lineNumber, "EQU symbol not defined.");
				
				if(!testEQUSymbolName((*pTokenList)[i-1]->label))
					throwError(21, (*pTokenList)[i]->filename, (*pTokenList)[i]->lineNumber, "Invalid symbol name.");
				
			}
			else if(i < 0) throwError(21, (*pTokenList)[i]->filename, (*pTokenList)[i]->lineNumber, "EQU symbol not defined.");
			
			if(i > 1)
			{
				if((*pTokenList)[i+1]->type != DIRECTIVE_PARAMETER)
					throwError(21, (*pTokenList)[i]->filename, (*pTokenList)[i]->lineNumber, "EQU value not defined.");
				
				if((*pTokenList)[i+1]->value == NULL)
					throwError(21, (*pTokenList)[i]->filename, (*pTokenList)[i]->lineNumber, "EQU parameter must be a numerical value.");
			}
			
			#ifdef DEBUG_L2
			printf("Processing EQU element...\nsymbol: %s\nvalue:%s\n\n", (*pTokenList)[i-1]->label, (*pTokenList)[i+1]->label);
			#endif
			
			//Replace all later occurances of the symbol with its set value
			for(unsigned int j = i + 2; j < *pTokenCount; j++)
			{
				if(!strcmp((*pTokenList)[j]->label, (*pTokenList)[i-1]->label))
				{
					(*pTokenList)[j]->value = (long *)malloc(sizeof(long));	//Because we will delete the original pointer once we delete the EQU line
					*((*pTokenList)[j]->value) = *((*pTokenList)[i+1]->value);
					
					(*pTokenList)[j]->valueType = (unsigned short *)malloc(sizeof(unsigned short));	//Because we will delete the original pointer once we delete the EQU line
					*((*pTokenList)[j]->valueType) = *((*pTokenList)[i+1]->valueType);
					
					//Also copy label (Even though not really needed); since its numerical.
					(*pTokenList)[j]->label = (char *)realloc((*pTokenList)[j]->label, strlen((*pTokenList)[i+1]->label));
					strcpy((*pTokenList)[j]->label, (*pTokenList)[i+1]->label);
					(*pTokenList)[j]->labelLength = (*pTokenList)[i+1]->labelLength;
					
					(*pTokenList)[j]->isValue = (*pTokenList)[i+1]->isValue;
					
					#ifdef DEBUG_L2
					printf("EQU replaced at %d\n", (*pTokenList)[j]->lineNumber);
					#endif
				}
			}
			//Delete the EQU line
			LEX_removeTokenAt(pTokenList, pTokenCount, i);	//EQU directive
			LEX_removeTokenAt(pTokenList, pTokenCount, i);	//Parameter
			LEX_removeTokenAt(pTokenList, pTokenCount, i);	//New line
			LEX_removeTokenAt(pTokenList, pTokenCount, i - 1);	//Preparameter
			
			i--; //To not skip next token on next iteration.
		}
	}
}

///This function first finds all defined macros in source code and puts them in a table (Array),
///then it replaces each macro with its macro body of tokenLists.
void PREPROC_processMacros(LEX_Token ***pTokenList, unsigned int *pTokenCount)
{
	LEX_tokenTable tt;
	tt.tokenList = *pTokenList;
	tt.tokenCount = *pTokenCount;
	
	//This table will contain all macros
	unsigned short macroCount = PREPROC_countMacros(tt);
	if(macroCount == 0) return;	//Save processing power
	
	PREPROC_Macro **macroTable = (PREPROC_Macro **)malloc(macroCount * sizeof(PREPROC_Macro *));
	
	//First we will fill the table
	for(unsigned int i = 0, j = 0; i < *pTokenCount; i++) 
	{
		if(compareCIS((*pTokenList)[i]->label, "MACRO") && (*pTokenList)[i]->type == DIRECTIVE)
		{
			macroTable[j] = (PREPROC_Macro *)malloc(sizeof(PREPROC_Macro));
			macroTable[j]->parameterList = (LEX_Token **)malloc(PREPROC_countMacroParameters(tt, i+1) * sizeof(LEX_Token *));
			
			if(i > 0)
				if((*pTokenList)[i-1]->type == DIRECTIVE_PREPARAMETER)
				{
					if(!testEQUSymbolName((*pTokenList)[i-1]->label)) throwError(41, (*pTokenList)[i-1]->filename, (*pTokenList)[i-1]->lineNumber, "Bad macro name.");
					macroTable[j]->label = (char *)malloc(strlen((*pTokenList)[i-1]->label));
					strcpy(macroTable[j]->label, (*pTokenList)[i-1]->label);
				}
				else throwError(40, (*pTokenList)[i]->filename, (*pTokenList)[i]->lineNumber, "Macro doesn't have a name.");
			else throwError(40, (*pTokenList)[i]->filename, (*pTokenList)[i]->lineNumber, "Macro doesn't have a name.");
			
			//Read macro parameters (Also remove their definition)
			unsigned int l = 0;
			for(unsigned int k = i+1; k < *pTokenCount && (*pTokenList)[k]->type != ENDL;)
			{
				if((*pTokenList)[k]->type == COMMA) 
				{
					LEX_removeTokenAt(pTokenList, pTokenCount, k);
					continue;
				}
				
				macroTable[j]->parameterList[l] = (LEX_Token *)malloc(sizeof(LEX_Token));
				LEX_copyToken(macroTable[j]->parameterList[l], (*pTokenList)[k]);
				LEX_removeTokenAt(pTokenList, pTokenCount, k);
				
				l++;
			}
			
			macroTable[j]->parameterCount = l;
			
			//Count tokens in macro
			//Set index after ENDL, that is the beginning of the macro's tokens
			unsigned int k = i;
			for(; (*pTokenList)[k]->type != ENDL && k < *pTokenCount; k++);
			k++;
			
			unsigned int mTStart = k;	//We will be using this later
			
			if(k >= *pTokenCount) throwError(42, (*pTokenList)[i]->filename, (*pTokenList)[i]->lineNumber, "Macro not closed with 'endm'");
			
			//Start counting
			unsigned int mTCount = 0;
			for(; k < *pTokenCount && (*pTokenList)[k]->type != DIRECTIVE && !compareCIS((*pTokenList)[k]->label, "ENDM"); k++) mTCount++;
			
			#ifdef DEBUG_L2
			printf("mTCount = %d\n", mTCount);
			#endif
			
			macroTable[j]->tokenList = (LEX_Token **)malloc(sizeof(LEX_Token *) * mTCount);
			macroTable[j]->tokenCount = mTCount;
			
			//Fill tokenList of macro (And delete each filled token, since we will be removing the macro definition later anyways)
			for(unsigned int u = 0; u < mTCount; u++)
			{
				macroTable[j]->tokenList[u] = (LEX_Token *)malloc(sizeof(LEX_Token));
				LEX_copyToken(macroTable[j]->tokenList[u], (*pTokenList)[mTStart]);
				LEX_removeTokenAt(pTokenList, pTokenCount, mTStart);
			}
				
			//Number of macros in table
			j++;
			
			//Remove what remains of macro definition (Macro name, the directive 'MACRO', and 'ENDM')
			LEX_removeTokenAt(pTokenList, pTokenCount, i);
			LEX_removeTokenAt(pTokenList, pTokenCount, i);
			LEX_removeTokenAt(pTokenList, pTokenCount, i);	//There is an ENDL after parameters
			LEX_removeTokenAt(pTokenList, pTokenCount, i);	//There is an ENDL after ENDM too :)
			LEX_removeTokenAt(pTokenList, pTokenCount, i-1);	//Macro name
		}
	}
	
	#ifdef DEBUG_L2
	printf("Macro list:\n");
	for(unsigned int i = 0; i < macroCount; i++)
		printf("%s\n", macroTable[i]->label);
	#endif
	
	//Now replace each occuring macro in code with its macro body tokenList
	for(unsigned int i = 0; i < *pTokenCount; i++)
	{	
		for(unsigned int j = 0; j < macroCount; j++)
		{
			if(!strcmp((*pTokenList)[i]->label, macroTable[j]->label) && (*pTokenList)[i]->type == LABEL)
			{
				//First we read its operands and place them in a tokenList IN ORDER.
				//But before that, count parameters to know how much memory we need to allocate
				unsigned short mPCount = 0;
				for(unsigned int k = i + 1; k < *pTokenCount && ((*pTokenList)[k]->type == LABEL || (*pTokenList)[k]->type == COMMA); k++)
				{
					if((*pTokenList)[k]->type == ENDL) break;
					if((*pTokenList)[k]->type == LABEL) mPCount++;
					
					
				}
					
				#ifdef DEBUG_L2
				printf("mPCount = %d\n", mPCount);
				#endif
					
				if(mPCount != macroTable[j]->parameterCount) throwError(43, (*pTokenList)[i]->filename, (*pTokenList)[i]->lineNumber, "Macro doesn't match definition.");
				
				LEX_Token **operandList = (LEX_Token **)malloc(sizeof(LEX_Token *) * mPCount);
				for(unsigned int k = i + 1, l = 0; k < *pTokenCount && ((*pTokenList)[k]->type == LABEL || (*pTokenList)[k]->type == COMMA); k++)
				{
					if((*pTokenList)[k]->type == LABEL)
					{
						#ifdef DEBUG_L2
						printf("Parameter %d = %s\n", l, (*pTokenList)[k]->label);
						#endif
				
						operandList[l] = (LEX_Token *)malloc(sizeof(LEX_Token));
						LEX_copyToken(operandList[l], (*pTokenList)[k]);
						operandList[l]->type = OPERAND;	//LEX_findTokenType() only stores OPERAND types if the previous token is INSTRUCTION, but this should also apply to macros.
														//However, LEX_findTokenType() can't know whether a macro is a macro or just a label.
						l++;
					}
				}
				
				#ifdef DEBUG_L2
				printf("Done copying operands of macro.\n");
				#endif
				
				
				//Now delete macro occurance and its operands as we will replace them
				for(unsigned int k = i; k < *pTokenCount; k++)
					if((*pTokenList)[i]->type != ENDL)
						LEX_removeTokenAt(pTokenList, pTokenCount, i);
					else break;
						
				LEX_removeTokenAt(pTokenList, pTokenCount, i); //Remove ENDL
				
				#ifdef DEBUG_L2
				printf("Done delete macro call.\n");
				#endif
				
				//Next we need to create a copy of the macro's original tokenlist, and make the changes we need according to the passed operands
				LEX_Token **patchedMacroBody = (LEX_Token **)malloc(sizeof(LEX_Token *) * macroTable[j]->tokenCount);
				for(unsigned int k = 0; k < macroTable[j]->tokenCount; k++)
				{
					patchedMacroBody[k] = (LEX_Token *)malloc(sizeof(LEX_Token));
					LEX_copyToken(patchedMacroBody[k], macroTable[j]->tokenList[k]);
				}
				
				#ifdef DEBUG_L2
				printf("Made a copy of macro table\n");
				#endif
				
				//Patch tokenlist according to passed operands
				for(unsigned int k = 0; k < macroTable[j]->tokenCount; k++)
				{
					long pos = LEX_findInTokenListCS_R(patchedMacroBody[k]->label, macroTable[j]->parameterList, macroTable[j]->parameterCount);
					
					if(pos >= 0)
					{
						//Replace operand in macro body with this one
						LEX_cleanToken(patchedMacroBody[k]);
						
						patchedMacroBody[k] = (LEX_Token *)malloc(sizeof(LEX_Token));
						LEX_copyToken(patchedMacroBody[k], operandList[pos]);
					}
				}
				
				#ifdef DEBUG_L2
				printf("Macro table patching done.\n");
				#endif
				
				//Finally, concatenate the patched macro body to the code
				LEX_Concatenate(pTokenList, pTokenCount, patchedMacroBody, macroTable[j]->tokenCount, i);
				
				//Cleanup
				LEX_cleanTokenList(operandList, mPCount);
			}
		}
	}
	
	//Cleanup
	for(unsigned int j = 0; j < macroCount; j++)
	{
		LEX_cleanTokenList(macroTable[j]->tokenList, macroTable[j]->tokenCount);
		LEX_cleanTokenList(macroTable[j]->parameterList, macroTable[j]->parameterCount);
		free(macroTable[j]);
	}
	free(macroTable);
}
