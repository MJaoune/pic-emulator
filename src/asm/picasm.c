/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "picasm.h"

///Takes source filename and returns compiled file buffer in "outBuffer" and returns its size.
size_t ASM_Compile(char *src, char *prefix, unsigned char **outBuffer)
{
	char srcFileName[300] = { '\0' };
	
	if(prefix != NULL)	
		for(size_t r = 0; r < strlen(prefix); r++)
			printf("%02X ", prefix[r]);
	
	if(prefix != NULL)
		strcat(srcFileName, prefix);
		
	strcat(srcFileName, src);
	
	printf("Compiler filename: %s\n", srcFileName);
	
	FILE *sourceFile = fopen(srcFileName, "rb");
	
	if(sourceFile == NULL)
	{
		printf("'%s' not found!\n", srcFileName);
		return 0;
	}
	
	char *srcFileName_Trimmed = (char *)calloc(sizeof(char), strlen(srcFileName));
	strcat(srcFileName_Trimmed, srcFileName);
	
	LEX_tokenTable tl = LEX_tokenizeFile(sourceFile, srcFileName);
	
	printf("Preproccessing source file...\n");
	
	//We need to extract the filepath, it is needed for when processing includes
	size_t fileNameLength = strlen(srcFileName);
	char *filePath = (char *)malloc(fileNameLength);
	size_t lastSlashIndex = 0;
	for(size_t i = 0; i < fileNameLength; i++)
		if(srcFileName[i] == '/' || srcFileName[i] == '\\')
			lastSlashIndex = i;
			
	if(lastSlashIndex > 0)
	{
		for(size_t i = 0; i <= lastSlashIndex; i++)
			filePath[i] = srcFileName[i];
			
		filePath[lastSlashIndex+1] = '\0';
	}
	else
	{
		free(filePath);
		filePath = NULL;
	}
	
	//Now start preprocessing
	PREPROC_processDirectives(&tl.tokenList, &tl.tokenCount, filePath);
	
	//Now compilation
	printf("Compiling...\n");
	
	size_t fSize = ASM_Assemble(tl.tokenList, tl.tokenCount, outBuffer);
	
	return fSize;
}
