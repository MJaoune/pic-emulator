/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "lexer.h"

unsigned int lineCounter;

///Convert file text content into tokens (tokenList returned).
LEX_tokenTable LEX_tokenizeFile(FILE *sourceFile, char *fileName)
{	
	#ifdef DEBUG_L2
	printf("Tokenizing file: %s\n", fileName);
	#endif
	
	fseek(sourceFile, 0, SEEK_END);
	size_t fileSize = ftell(sourceFile);
	fseek(sourceFile, 0, SEEK_SET);
	
	char *buffer = (char *)malloc(fileSize);
	for(unsigned int i = 0; i < fileSize; i++) buffer[i] = fgetc(sourceFile);
	
	fclose(sourceFile);
	
	#ifdef DEBUG_L2
	printf("File size is %ld.\n", fileSize);
	#endif

	//First we pretend the Walk, to get the number of tokens, in order to malloc correctly.
	unsigned int tokenCount = LEX_Walk(buffer, fileSize, NULL, true, NULL);
	
	LEX_Token **tokenList = (LEX_Token **)malloc(tokenCount * sizeof(LEX_Token *));
	
	//Now the Walk is real (pretend = false).
	tokenCount = LEX_Walk(buffer, fileSize, &tokenList, false, fileName);
	
	free(buffer);
	
	LEX_tokenTable tokenTable;
	tokenTable.tokenList = tokenList;
	tokenTable.tokenCount = tokenCount;
	
	return tokenTable;
}

///This function walks on the buffer and sends "Words" to the lexer
///Pretend doesn't store anything in the tokenList, it is not even touched (So NULL can be used).
unsigned int LEX_Walk(char *buffer, unsigned int bufferSize, LEX_Token ***tokenList, bool pretend, char *filename)
{
	lineCounter = 1;
	unsigned int tokenCounter = 0;
	char endFlag = 0;
	
	for(unsigned int i = 0; i < bufferSize;)
	{
		unsigned int j = i;
		unsigned int r = 0;
		char word[WORD_LENGTH+2];
		unsigned short wordLength = 0;
		for(;; j++, r++)
		{
			if(r == WORD_LENGTH+1)
			{
				throwError(1, filename, lineCounter, "Words can't be longer than 50 characters!");
				break;
			}
			
			//Comment
			if(buffer[j] == ';')
			{
				tokenCounter = LEX_Lex(word, wordLength, tokenList, tokenCounter, pretend, filename);
				
				//We need to include new lines in token list (Only if comment is not on a separate line)
				if(j != 0) 
				{
					if(buffer[j - 1] != '\n') 
					{
						char newl[1] = {'\n'};
						tokenCounter = LEX_Lex(newl, 1, tokenList, tokenCounter, pretend, filename);
					}
				}	
				
				//Now skip rest of line
				int k = 1;
				for(; buffer[j + k] != '\n' && buffer[j + k] != EOF; k++);
				i += k + 1;
				
				r = 0;
				
				lineCounter++;
				
				break;
			}
			
			if(buffer[j] == EOF) 
			{
				tokenCounter = LEX_Lex(word, wordLength, tokenList, tokenCounter, pretend, filename);
				break; 
			}
			
			if(buffer[j] == 0xA || buffer[j] == 0xD) 
			{
				tokenCounter = LEX_Lex(word, wordLength, tokenList, tokenCounter, pretend, filename);
				
				//We need to include new lines in token list
				char newl[1] = {0xA};
				tokenCounter = LEX_Lex(newl, 1, tokenList, tokenCounter, pretend, filename);
				r++;
				
				if(buffer[j] == 0xD) {
					i++;
					j++;
				}
				lineCounter++;
				
				#ifdef DEBUG_L2
				if(!pretend) printf("Line %d:\n", lineCounter);
				#endif
				break; 
			}
			
			if(buffer[j] == ',') 
			{
				tokenCounter = LEX_Lex(word, wordLength, tokenList, tokenCounter, pretend, filename);
				
				//We also need to include commas in token list
				char comma[1] = {','};
				tokenCounter = LEX_Lex(comma, 1, tokenList, tokenCounter, pretend, filename);
				r++;
				break; 
			}
			
			if(buffer[j] == ' ' || buffer[j] == '\t')
			{
				tokenCounter = LEX_Lex(word, wordLength, tokenList, tokenCounter, pretend, filename);
				r++;
				break;
			}
			
			word[r] = buffer[j];
			wordLength++;
		}

		i += r;
	}
	
	return tokenCounter;
}

///Processes word be word sent by the walker function "LEX_Walk" and creates a token list
unsigned int LEX_Lex(char *word, unsigned short wordLength, LEX_Token ***tokenList, unsigned int tokenCounter, bool pretend, char *filename)
{	
	if(wordLength == 0) return tokenCounter;	//Maybe found a single line comment or multiple newlines/spaces
	
	//Create a new token to be placed in tokenList
	if(!pretend)
	{
		LEX_Token *newToken = (LEX_Token *)malloc(sizeof(LEX_Token));
		
		//Fill label
		newToken->label = (char *)malloc(wordLength + 1);
		for(unsigned short i = 0; i < wordLength; i++) newToken->label[i] = word[i];
		
		//Add null terminator for token's label
		newToken->label[wordLength] = '\0';
		
		#ifdef DEBUG_L2_L2
		printf("Token label: %s\n", newToken->label);
		#endif
		
		newToken->labelLength = wordLength + 1;
		
		//Store filename
		unsigned int fnameLength = strlen(filename) + 1;
		newToken->filename = (char *)malloc(fnameLength);
		//for(unsigned int i = 0; i < fnameLength; i++) newToken->filename[i] = filename[i];
		strcpy(newToken->filename, filename);
		
		//Find type and store it
		#ifdef DEBUG_L2_L2
		printf("\tFinding type...\n");
		#endif
		
		newToken->type = LEX_findTokenType(*newToken, tokenList, tokenCounter);
		
		#ifdef DEBUG_L2_L2
		printf("\tType found: ");
		#endif
		
		#ifdef DEBUG_L2_L2
		{
			char *typeStr = (char*)malloc(20);
			LEX_tokenTypeToString(*newToken, &typeStr);
			printf("%s\n", typeStr);
			//WARNING: MEMORY LEAK!
		}
		#endif
		
		newToken->isValue = false;
		
		newToken->lineNumber = lineCounter;	//Important for error reporting later.
		
		//Store token in list
		#ifdef DEBUG_L2_L2
		printf("\tStoring token in tokenList.\n");
		#endif
		
		(*tokenList)[tokenCounter] = newToken;
		
		#ifdef DEBUG_L2_L2
		printf("\tStored token in tokenList.\n");
		#endif
		
		//Attempt post fixes (If needed)
		LEX_attemptPostFixes(tokenList, tokenCounter);
		
		#ifdef DEBUG_L2_L2
		printf("\tFinished post fixes.\n");
		#endif
		
		#ifdef DEBUG_L2_L2
		printf("************\n");
		#endif
	}
	
	tokenCounter++;
	return tokenCounter;
}

///Detects tokenType by checking the token label in addition to previous tokens (If needed)
LEX_tokenType LEX_findTokenType(LEX_Token t, LEX_Token ***tokenList, unsigned int tokenCount)
{
	if(LEX_isInstruction(t.label)) return INSTRUCTION;
	else if(LEX_isDirective(t.label)) return DIRECTIVE;
	else if(t.label[0] == 0xA) return ENDL;
	else if(t.label[0] == ',') return COMMA;
	
	//Check previous token and assume type according to that
	else if(tokenCount > 0) {
		LEX_Token *prevT = (*tokenList)[tokenCount - 1]; //Previous token
		if(prevT->type == INSTRUCTION) return OPERAND;
		else if(prevT->type == DIRECTIVE) return DIRECTIVE_PARAMETER;
		
		else if(tokenCount > 1) {
			LEX_Token *prevprevT = (*tokenList)[tokenCount - 2];
			
			if(prevT->type == COMMA) {
				//This is for MACROs parameters, since they are variable
				if(prevprevT->type == DIRECTIVE_PARAMETER)
					return DIRECTIVE_PARAMETER;
				
				//This is for normal instructions
				else if(prevprevT->type == OPERAND)
					return OPERAND;
			}
		}
	}
	
	return LABEL;
}

///Checks if token label represents an instruction
bool LEX_isInstruction(const char *label)
{
	//Check if it is found in our predefined arrays
	if(findInArrayCIS(label, LEX_instructionsFD, FD_INSTRUCTIONS_COUNT)) return true;
	if(findInArrayCIS(label, LEX_instructionsF, F_INSTRUCTIONS_COUNT)) return true;
	if(findInArrayCIS(label, LEX_instructionsFB, FB_INSTRUCTIONS_COUNT)) return true;
	if(findInArrayCIS(label, LEX_instructionsK, K_INSTRUCTIONS_COUNT)) return true;
	if(findInArrayCIS(label, LEX_instructionsNOP, NOP_INSTRUCTIONS_COUNT)) return true;
	
	return false;
}

///Checks if token is an instruction/directive that requires an address (e.g. CALL, GOTO, ORG)
bool LEX_requiresAddress(LEX_Token t)
{
	if(findInArrayCIS(t.label, LEX_requireADDR, ADDR_TOKENS_COUNT)) return true;
	
	return false;
}

///Checks if token label represents a directive
bool LEX_isDirective(const char *label)
{
	if(findInArrayCIS(label, LEX_directives, DIRECTIVE_COUNT)) return true;
	
	return false;
}

///Sometimes we assume a wrong token type until we know the next one, this function fixes the list accordingly
void LEX_attemptPostFixes(LEX_Token ***tokenList, unsigned int tokenCount)
{
	if(tokenCount < 2) return;
	for(unsigned int i = 0; i < tokenCount; i++)
	{
		LEX_Token *t = (*tokenList)[i];
		
		//In the case of EQU or MACRO, we need to fix previous token from "LABEL" to "DIRECTIVE_PREPARAMETER"
		if(compareCIS(t->label, "EQU") || compareCIS(t->label, "MACRO")) 
		{
			//This check is to prevent memory access error.
			if(i > 0)
			{
				LEX_Token *prevT = (*tokenList)[i - 1];
				prevT->type = DIRECTIVE_PREPARAMETER;
			}
		}
	}
}

///Used mainly for debugging
void LEX_tokenTypeToString(LEX_Token t, char **string)
{
	switch(t.type)
	{
		case DIRECTIVE:
			strcpy(*string, "DIRECTIVE");
			break;
		case DIRECTIVE_PREPARAMETER:
			strcpy(*string, "DIRECTIVE_PREPARAMETER");
			break;
		case DIRECTIVE_PARAMETER:
			strcpy(*string, "DIRECTIVE_PARAMETER");
			break;
		case LABEL:
			strcpy(*string, "LABEL");
			break;
		case INSTRUCTION:
			strcpy(*string, "INSTRUCTION");
			break;
		case OPERAND:
			strcpy(*string, "OPERAND");
			break;
		case ENDL:
			strcpy(*string, "ENDL");
			break;
		case COMMA :
			strcpy(*string, "COMMA");
			break;
		default:
			strcpy(*string, "UNKNOWN");
	}
}

///Merges two tokenlists at set index (Used mainly for #include directive). Changes size automatically.
void LEX_Concatenate(LEX_Token ***dst, unsigned int *dstSize, LEX_Token **src, unsigned int srcSize, unsigned int index)
{
	#ifdef DEBUG_L2
	printf("Concatenating two tokenLists...\n");
	#endif
	
	//This check is important since we will be using this function in a recursive function.
	if(dst == NULL) return;
	
	//Increase allocated memory
	*dst = (LEX_Token **)realloc(*dst, (*dstSize + srcSize) * sizeof(LEX_Token *));
	
	#ifdef DEBUG_L2
	printf("Reallocation successful from size %d to %d.\n", *dstSize, (*dstSize + srcSize));
	#endif
	
	//Make space for new content and append it
	for(unsigned int i = *dstSize; i > index; i--)
		(*dst)[i - 1 + srcSize] = (*dst)[i-1];
	
	for(unsigned int i = 0; i < srcSize; i++)
		(*dst)[i + index] = src[i];
		
	*dstSize += srcSize;
		
	
	#ifdef DEBUG_L2
	printf("Concatenation successful.\n");
	#endif
}

void LEX_removeTokenAt(LEX_Token ***tokenList, unsigned int *tokenCount, unsigned int index)
{
	if(*tokenCount <= 0) return;
	
	//free((*tokenList)[index]);
	LEX_cleanToken((*tokenList)[index]);
	
	(*tokenCount)--;
	
	for(unsigned int i = index; i < *tokenCount; i++)
		(*tokenList)[i] = (*tokenList)[i+1];
		
	(*tokenList) = (LEX_Token **)realloc(*tokenList, *tokenCount * sizeof(LEX_Token *));
}

///Frees token memory
void LEX_cleanToken(LEX_Token *token)
{
	if(token == NULL) return;	//Already cleaned (Or missing?)
	
	
	if(token->label != NULL)
		free(token->label);
	
	if(token->filename != NULL)
		free(token->filename);
	
	if(token->isValue)
	{
		if(token->value != NULL)
			free(token->value);
			
		if(token->valueType != NULL)
			free(token->valueType);
	}
		
	
	free(token);
}

///Free tokenList
void LEX_cleanTokenList(LEX_Token **tokenList, unsigned int tokenCount)
{
	for(unsigned int i = 0; i < tokenCount; i++)
		LEX_cleanToken(tokenList[i]);
	
	free(tokenList);
}

///Copies values of src to dst. Doesn't do memory allocation for token.
void LEX_copyToken(LEX_Token *dst, LEX_Token *src)
{
	if(src == NULL || dst == NULL) return;
	
	dst->label = (char *)malloc(strlen(src->label));
	strcpy(dst->label, src->label);
	
	dst->type = src->type;
	
	if(src->filename != NULL)
	{
		dst->filename = (char *)malloc(strlen(src->filename));
		strcpy(dst->filename, src->filename);
	}
	
	dst->lineNumber = src->lineNumber;
	dst->labelLength = src->labelLength;
	dst->isValue = src->isValue;
	
	if(src->isValue)
	{
		if(src->value != NULL)
		{
			dst->value = (long *)malloc(sizeof(long));
			*(dst->value) = *(src->value);
		}
	
		if(src->valueType != NULL)
		{
			dst->valueType = (unsigned short *)malloc(sizeof(unsigned short));
			*(dst->valueType) = *(src->valueType);
		}
	}
}

///Identical to findInArrayCS_R(), but is tokenList compatible.
long LEX_findInTokenListCS_R(const char *word, LEX_Token **arr, unsigned int tokenCount)
{
	for(unsigned int i = 0; i < tokenCount; i++)
		if(!strcmp(word, arr[i]->label))
			return i;
	
	return -1;
}
