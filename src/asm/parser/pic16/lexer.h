/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef _H_LEXER_
#define _H_LEXER_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "../../util.h"
#include "../../error.h"

#define WORD_LENGTH 50

typedef enum LEX_tokenType {
	DIRECTIVE = 1,
	DIRECTIVE_PREPARAMETER = 2, //Such as in the case of "var_name equ 43"
	DIRECTIVE_PARAMETER = 4,	//e.g. org 0x100
	LABEL = 8,
	INSTRUCTION = 16,
	OPERAND = 32,
	ENDL = 64,	//Line end (Useful for tokens that don't take anything after them)
	COMMA = 128
} LEX_tokenType;

typedef enum LEX_instructionType {
	FD_INSTRUCTION,	///Takes 2 operands: (f, d)
	F_INSTRUCTION, ///Takes 1 operand: (f)
	FB_INSTRUCTION, ///Takes 2 operands: (f, b)
	K_INSTRUCTION, ///Takes 1 operand: (k)
	NOP_INSTRUCTION, ///Takes no operands
} LEX_instructionType;

typedef struct LEX_Token {
	LEX_tokenType type;
	char *label;
	unsigned int labelLength;
	unsigned int lineNumber; //Line number in which the token was found (Useful for error reporting.)
	char *filename; //The name of the file that contains this token
	
	bool isValue;	//Only set to true if numerical value (After processOperandValues())
	//Below is only for numerical values (They are made pointers to preserve memory in case they were never used)
	long *value; //This is only used after processOperandValues() if token is a numerical value
	unsigned short *valueType; //1: positive numerical. 2: negative numerical. Other than that, ASCII.
} LEX_Token;

//The token table only contains the number of tokens in the tokenList.
typedef struct LEX_tokenTable {
	LEX_Token **tokenList;
	unsigned int tokenCount;
} LEX_tokenTable;

extern unsigned int lineCounter;	///Counts line position when lexing

#define DIRECTIVE_COUNT 7
extern const char *LEX_directives[DIRECTIVE_COUNT];

///These instructions take 2 operands (f, d)
#define FD_INSTRUCTIONS_COUNT 14
extern const char *LEX_instructionsFD[FD_INSTRUCTIONS_COUNT];

///These instructions take 1 operand (f)
#define F_INSTRUCTIONS_COUNT 2
extern const char *LEX_instructionsF[F_INSTRUCTIONS_COUNT];

///These instructions take 2 operands (f, b)
#define FB_INSTRUCTIONS_COUNT 4
extern const char *LEX_instructionsFB[FB_INSTRUCTIONS_COUNT];

///These instructions take 1 operand (k)
#define K_INSTRUCTIONS_COUNT 9
extern const char *LEX_instructionsK[K_INSTRUCTIONS_COUNT];

///These instructions take no operands
#define NOP_INSTRUCTIONS_COUNT 6
extern const char *LEX_instructionsNOP[NOP_INSTRUCTIONS_COUNT];

///Address requiring instructions/directives
#define ADDR_TOKENS_COUNT 3
extern const char *LEX_requireADDR[ADDR_TOKENS_COUNT];

LEX_tokenTable LEX_tokenizeFile(FILE *sourceFile, char *fileName);
unsigned int LEX_Walk(char *buffer, unsigned int bufferSize, LEX_Token ***tokenList, bool pretend, char *filename);
unsigned int LEX_Lex(char *word, unsigned short wordLength, LEX_Token ***tokenList, unsigned int tokenCount, bool pretend, char *filename);
LEX_tokenType LEX_findTokenType(LEX_Token t, LEX_Token ***tokenList, unsigned int tokenCount);
bool LEX_isInstruction(const char *label);
bool LEX_requiresAddress(LEX_Token t);
bool LEX_isDirective(const char *label);
void LEX_attemptPostFixes(LEX_Token ***tokenList, unsigned int tokenCount);
void LEX_tokenTypeToString(LEX_Token t, char **string);
void LEX_Concatenate(LEX_Token ***dst, unsigned int *dstSize, LEX_Token **src, unsigned int srcSize, unsigned int index);
void LEX_removeTokenAt(LEX_Token ***tokenList, unsigned int *tokenCount, unsigned int index);
void LEX_cleanToken(LEX_Token *token);
void LEX_cleanTokenList(LEX_Token **tokenList, unsigned int tokenCount);
void LEX_copyToken(LEX_Token *dst, LEX_Token *src);
long LEX_findInTokenListCS_R(const char *word, LEX_Token **arr, unsigned int tokenCount);

#endif //_H_LEXER_
