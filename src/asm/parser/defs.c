/*
Copyright (c) 2019, Mahmoud al-Jaoune
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAHMOUD AL-JAOUNE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
 
#ifdef PIC_MODEL_16
#include "pic16/lexer.h"
#endif

#ifdef PIC_MODEL_32
#include "pic32/lexer.h"
#endif

#ifdef PIC_MODEL_16
const char *LEX_directives[] = {"LIST", "ORG", "#INCLUDE", "EQU", "END", "MACRO", "ENDM"};
#endif

///These instructions take 2 operands (f, d)
#ifdef PIC_MODEL_16
const char *LEX_instructionsFD[] = 
{
	"ADDWF",
	"ANDWF",
	"COMF",
	"DECF",
	"DECFSZ",
	"INCF",
	"INCFSZ",
	"IORWF",
	"MOVF",
	"RLF",
	"RRF",
	"SUBWF",
	"SWAPF",
	"XORWF"
};
#endif

///These instructions take 1 operand (f)
#ifdef PIC_MODEL_16
const char *LEX_instructionsF[] = 
{
	"CLRF",
	"MOVWF"
};
#endif

///These instructions take 2 operands (f, b)
#ifdef PIC_MODEL_16
const char *LEX_instructionsFB[] = 
{
	"BCF",
	"BSF",
	"BTFSC",
	"BTFSS"
};
#endif

///These instructions take 1 operand (k)
#ifdef PIC_MODEL_16
const char *LEX_instructionsK[] = 
{
	"ADDLW",
	"ANDLW",
	"CALL",
	"GOTO",
	"IORLW",
	"MOVLW",
	"RETLW",
	"SUBLW",
	"XORLW"
};
#endif

///These instructions take no operands
#ifdef PIC_MODEL_16
const char *LEX_instructionsNOP[] = 
{
	"CLRW",
	"NOP",
	"CLRWDT",
	"RETFIE",
	"RETURN",
	"SLEEP"
};

///These instructions/directives require an address as an operand
const char *LEX_requireADDR[] = 
{
	"GOTO",
	"CALL",
	"ORG"
};
#endif
