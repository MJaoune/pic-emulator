# MJaoune's PIC Emulator

A PIC16 Assembler & Emulator (Interpreter) written in plain C from scratch without relying on any external libraries.

## How to build

To build the project from source, you need autotools (Which is installed by default on most POSIX systems) and of course GCC. After that just execute the following commands:

```
autoreconf -fi
./configure
make
```
You may also build with debugging output enabled (Verbose):

```
./configure --with-debug
make
```

## How to run

Just execute the executable `picemu` through CLI and pass your PIC16 Assembly source code file path to it as follows:

```
./picemu myprogram.asm
```
