;Mahmoud Jaoune
;Indirect addressing example (Writes from 0x1 to 0xA at address range 0x22 to 0x0B)

FSR equ 0x04

movlw 1
movwf 0x20

movlw 10
movwf 0x21

movlw 0x22
movwf FSR

loop
;Take counter value
movf 0x20

movwf 0x00
incf FSR, 1

incf 0x20, 1

decfsz 0x21,1
goto loop
nop
