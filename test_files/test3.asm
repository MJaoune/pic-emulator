;Mahmoud Jaoune
;EEPROM write (Run picemu with 'eeprom' argument to print EEPROM contents)

EECON EQU 0x08
EEDATA EQU 0x08
EEADR EQU 0x09
INTCON EQU 0x0B
STATUS EQU 0x03

;I don't want EEPROM interrupt (Or any other interrupt)
movlw 0x00
movwf INTCON

;=======Write 1========

;Set data we want to send
movlw 0x10
movwf EEDATA
movlw 0
movwf EEADR

;Go to bank 1
movlw 0x20
iorwf STATUS, 1

;Initiate data write
movlw 2
iorwf EECON, 1

;=======Write 2========

;Go to bank 0
movlw 0xDF
andwf STATUS, 1

;Set data we want to send
movlw 0x20
movwf EEDATA
movlw 1
movwf EEADR

;Go to bank 1
movlw 0x20
iorwf STATUS, 1

;Initiate data write
movlw 2
iorwf EECON, 1


;=======Write 3========


;Go to bank 0
movlw 0xDF
andwf STATUS, 1

;Set data we want to send
movlw 0x30
movwf EEDATA
movlw 2
movwf EEADR

;Go to bank 1
movlw 0x20
iorwf STATUS, 1

;Initiate data write
movlw 2
iorwf EECON, 1

end
