;Mahmoud Jaoune
;Fibonacci

counter EQU 0x15
Fib0 EQU 0x20
Fib1 EQU 0x21
Fib2 EQU 0x22
Fibtemp EQU 0x23

;5 sums
movlw 5
movwf counter

clrf Fib0
movlw 1
movwf Fib1
movwf Fib2

forward
	movf Fib1, 0
	addwf Fib2, 0
	movwf Fibtemp

	movf Fib1, 0
	addwf Fib2, 0
	movwf Fibtemp

	movf Fib1, 0
	movwf Fib0
	movf Fib2, 0
	movwf Fib1
	movf Fibtemp, 0
	movwf Fib2

	decfsz counter, 1
goto forward

end
